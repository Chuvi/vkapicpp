#ifndef MD5_H_INCLUDED
#define MD5_H_INCLUDED
#define HAVE_MD5
#define MD5_DIGEST_LENGTH 16
#define MD5_DIGEST_LENGTH_64 2 //(MD5_DIGEST_LENGTH_64/sizeof(uint64_t))
#define MD5_STRING_LENGTH (MD5_DIGEST_LENGTH*2+1)
typedef unsigned char *POINTER;

/* UINT2 defines a two byte word */
typedef unsigned short int UINT2;

/* UINT4 defines a four byte word */
typedef unsigned long int UINT4;


typedef struct
{
	UINT4 state[4];                                   /* state (ABCD) */
	UINT4 count[2];        /* number of bits, modulo 2^64 (lsb first) */
	unsigned char buffer[64];                         /* input buffer */
} MD5_CTX;

#ifdef __cplusplus
extern "C"
{
#endif
   void MD5_String(const char* in_szString, unsigned char out_buf[MD5_DIGEST_LENGTH]);


   // Low level API
   void MD5Init(MD5_CTX *);
   void MD5Update(MD5_CTX *,const void *, unsigned int);
   void MD5UpdateConst(MD5_CTX *context, unsigned char input, unsigned int inputLen);
   void MD5Final(unsigned char[16], MD5_CTX *);
   void MD5_Buffer(const void* in_pBuffer, unsigned long in_ulNbBytes, unsigned char out_buf[MD5_DIGEST_LENGTH]);
   void MD5_Const(unsigned char in, unsigned long in_ulNbBytes, unsigned char out_buf[MD5_DIGEST_LENGTH]);
   void MD5_BufferToString(const void* in_pBuffer, unsigned long in_ulNbBytes, char out_str[MD5_STRING_LENGTH]);
   void MD5_ConstToString(unsigned char in, unsigned long in_ulNbBytes, char out_str[MD5_STRING_LENGTH]);
#ifdef __cplusplus
}
#endif

#endif /* MD5_H_INCLUDED */
