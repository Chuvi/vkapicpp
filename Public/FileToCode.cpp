#include "lzss.h"
#include "FileToCode.h"
#include <fstream>
#include <sstream>
#include <iomanip>
#include <vector>
#include <string>
#include "md5.h"
#include "checksum_crc.h"
#include <algorithm>

std::string File2Code(const char *szFileName, const char *szTargetName)
{

 
   std::ifstream fIn(szFileName,std::ios::binary);
   int CompressedSz = 0;
   bool bCompressed = true;
   std::stringstream stOut;
   if(!fIn.is_open()||fIn.fail())
   {
      return false;
   }
   std::vector<uint8_t> FileData;
   fIn.seekg(0, std::ios_base::end);
   auto FileSz = fIn.tellg();
   FileData.resize(FileSz);
   fIn.seekg(0, std::ios_base::beg);
   fIn.read(reinterpret_cast<char*>(&FileData[0]), FileSz);
   fIn.close();

   if(FileData.empty())
   {
      return false;
   }
   
   auto vCompr = LZSS_VCompress(FileData);
   
   if(vCompr.empty())
   {
      vCompr.insert(vCompr.end(), FileData.begin(), FileData.end());
    
      bCompressed = false;
   }
  
   auto vUncom = LZSS_VUncompress(vCompr);
  
   if(vUncom != FileData)
   {
      return stOut.str();
   }
   stOut << "/*" << std::endl;
   stOut << "typedef struct FileData_s" << std::endl;
   stOut << "{" << std::endl;
   stOut << "\tvoid *pData;" << std::endl;
   stOut << "\tuint32_t nDataSz;" << std::endl;
   stOut << "\tuint32_t nBufSz;" << std::endl;
   stOut << "\tuint32_t CRC32;" << std::endl;
   stOut << "\tuint64_t MD5[2];" << std::endl;
   stOut << "\tbool bCompressed;" << std::endl;
   stOut << "}FileData_t; " << std::endl;
   stOut << "*/" << std::endl;
   size_t nNumWords = 0;
#if 0
   stOut << "char " << szTargetName << "_data_c[]=\n\t\"";
  

   bool bPrintHex = false;
   for (auto Byte : vCompr)
   {

      nNumWords++;
      if (nNumWords > 200)
      {
         stOut << "\",\n\t\"";
         nNumWords = 0;
      }
      if ((!(bPrintHex&&isxdigit(Byte))) && isprint(Byte) && Byte != '\"'&&Byte != '\r'&&Byte != '\n'&&Byte != '\\')
      {
         bPrintHex = false;
         stOut << Byte;
      }
      else
      {
         bPrintHex = true;
         stOut << "\\x" << std::setfill('0') << std::setw(2)<< std::hex << (uint32_t)Byte;
      }
   }
   stOut << "\";\n";
#endif
   stOut << "uint64_t " << szTargetName << "_data[]=\n{\n\t";
   FileData_s svFileData;
   size_t nCur = 0;
   
   size_t nBytesLeft = vCompr.size();
   uint64_t nLastVal=0;
   nNumWords = 0;
   do
   {
      stOut << "0x" << std::setfill('0') << std::setw(sizeof(uint64_t) * 2) << std::hex << reinterpret_cast<uint64_t*>(&vCompr[0])[nCur] << ", ";
      nCur++;
      nBytesLeft -= sizeof(uint64_t);
      if(++nNumWords > 12)
      {
         nNumWords = 0;
         stOut << "\n\t";
      }
   }
   while(nBytesLeft >= sizeof(uint64_t));
   memcpy(&nLastVal, &reinterpret_cast<uint64_t*>(&vCompr[0])[nCur], nBytesLeft);
   stOut << "0x" << std::setfill('0') << std::setw(sizeof(uint64_t) * 2) << std::hex << nLastVal << "\n};\n";
   svFileData.nDataSz = FileData.size();
   svFileData.nBufSz = vCompr.size();
   svFileData.bCompressed = bCompressed;

   MD5_CTX MD5Ctx;
   MD5Init(&MD5Ctx);
   MD5Update(&MD5Ctx, &FileData[0], FileData.size());
   MD5Update(&MD5Ctx, &svFileData.nDataSz, sizeof(svFileData.nDataSz));
   MD5Update(&MD5Ctx, &svFileData.nBufSz, sizeof(svFileData.nBufSz));
   MD5Final(reinterpret_cast<unsigned char*>(&svFileData.MD5), &MD5Ctx);

   CRC32_Init(reinterpret_cast<CRC32_t*>(&svFileData.CRC32));
   CRC32_ProcessBuffer(reinterpret_cast<CRC32_t*>(&svFileData.CRC32), &FileData[0], FileData.size());
   CRC32_ProcessBuffer(reinterpret_cast<CRC32_t*>(&svFileData.CRC32), &svFileData.nDataSz, sizeof(svFileData.nDataSz));
   CRC32_ProcessBuffer(reinterpret_cast<CRC32_t*>(&svFileData.CRC32), &svFileData.nBufSz, sizeof(svFileData.nBufSz));
   CRC32_Final(reinterpret_cast<CRC32_t*>(&svFileData.CRC32));
   stOut << "FileData_t _" << szTargetName << "={ "
      << szTargetName << "_data, "
      << "0x" << std::setfill('0') << std::setw(sizeof(uint32_t) * 2) << std::hex << svFileData.nDataSz << ", "
      << "0x" << std::setfill('0') << std::setw(sizeof(uint32_t) * 2) << std::hex << svFileData.nBufSz << ", "
      << "0x" << std::setfill('0') << std::setw(sizeof(uint32_t) * 2) << std::hex << svFileData.CRC32 << ", "
      << "{0x" << std::setfill('0') << std::setw(sizeof(uint64_t) * 2) << std::hex << svFileData.MD5[0] << ", "
      << "0x" << std::setfill('0') << std::setw(sizeof(uint64_t) * 2) << std::hex << svFileData.MD5[1] << "}, "
      <<(bCompressed?"true":"false")<<"};\n";
   return stOut.str();
}

std::vector<uint8_t> Code2Data(const FileData_t *FileData)
{
  
   std::vector<uint8_t> vOut;
   if(!FileData)
   {
      return vOut;
   }
   if(!FileData->bCompressed)
   {
      vOut.insert(vOut.end(), reinterpret_cast<uint8_t*>(FileData->pData), reinterpret_cast<uint8_t*>(FileData->pData) + FileData->nDataSz);
   }
   else
   {
      auto vUncompr = LZSS_VUncompress(std::vector<uint8_t>(reinterpret_cast<uint8_t*>(FileData->pData), reinterpret_cast<uint8_t*>(FileData->pData) + FileData->nBufSz));
      if(!vUncompr.empty())
      {
         vOut.insert(vOut.end(), vUncompr.begin(), vUncompr.end());
      }
     
   }

   uint32_t CRC32;
   uint64_t MD5[2];
   MD5_CTX MD5Ctx;
   if(!vOut.empty())
   {


      MD5Init(&MD5Ctx);
      MD5Update(&MD5Ctx, &vOut[0], vOut.size());
      MD5Update(&MD5Ctx, &FileData->nDataSz, sizeof(FileData->nDataSz));
      MD5Update(&MD5Ctx, &FileData->nBufSz, sizeof(FileData->nBufSz));
      MD5Final(reinterpret_cast<unsigned char*>(&MD5), &MD5Ctx);

      CRC32_Init(reinterpret_cast<CRC32_t*>(&CRC32));
      CRC32_ProcessBuffer(reinterpret_cast<CRC32_t*>(&CRC32), &vOut[0], vOut.size());
      CRC32_ProcessBuffer(reinterpret_cast<CRC32_t*>(&CRC32), &FileData->nDataSz, sizeof(FileData->nDataSz));
      CRC32_ProcessBuffer(reinterpret_cast<CRC32_t*>(&CRC32), &FileData->nBufSz, sizeof(FileData->nBufSz));
      CRC32_Final(reinterpret_cast<CRC32_t*>(&CRC32));
      if(vOut.size()!=FileData->nDataSz|| MD5[0] != FileData->MD5[0] || MD5[1] != FileData->MD5[1] || CRC32 != FileData->CRC32)
      {
         vOut.clear();
      }
   }
   return vOut;

}