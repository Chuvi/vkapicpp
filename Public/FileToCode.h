#include <string>
#include <vector>
#include <stdint.h>
#ifndef FileToCode_h__
#define FileToCode_h__

typedef struct FileData_s
{
   void *pData;
   uint32_t nDataSz;
   uint32_t nBufSz;
   uint32_t CRC32;
   uint64_t MD5[2];
   bool bCompressed;
}FileData_t;
std::string File2Code(const char *szFileName, const char *szTargetName);
std::vector<uint8_t> Code2Data(const FileData_t *FileData);
//HMODULE Code2Lib(const FileData_t *FileData);
#endif // FileToCode_h__
