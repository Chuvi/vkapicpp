#ifndef CHECKSUM_CRC_H
#define CHECKSUM_CRC_H

#ifdef _WIN32
#pragma once
#endif

#define HAVE_CRC32
typedef unsigned long CRC32_t;

#define NUM_BYTES 256
extern const CRC32_t pulCRCTable[ NUM_BYTES ];
#ifdef __cplusplus
extern "C"
{
#endif
   void CRC32_Init(CRC32_t *pulCRC);
   void CRC32_ProcessBuffer(CRC32_t *pulCRC, const void *p, int len);
   void CRC32_Final(CRC32_t *pulCRC);
   void CRC32_ProcessByte(CRC32_t *pulCRC, unsigned char ch);
   CRC32_t CRC32_ProcessSingleBuffer(void *p, int len);
#ifdef __cplusplus
}
#endif
#endif // CHECKSUM_CRC_H
