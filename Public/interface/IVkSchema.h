#ifndef IVkSchema_h__
#define IVkSchema_h__
#include <string>
#include <rapidjson/document.h>
#include <rapidjson/schema.h>


class IVkSchema
{
protected:
   virtual ~IVkSchema()
   {}
public:
   IVkSchema()
   {}
   
   virtual bool IsInitialised() const  = 0;
   virtual const rapidjson::Document *GetDocument(const std::string &sName)const = 0;
   virtual const rapidjson::SchemaDocument *GetSchema(const std::string &sName)const = 0;
   virtual const rapidjson::SchemaValidator *GetValidator(const std::string &sName)const = 0;
   virtual bool MakeSchemasCodeFiles(const std::string &sOutName) const = 0;
};

const IVkSchema *GetVKSchema();
#endif // IVkSchema_h__
