#ifndef IVkApi_h__
#define IVkApi_h__
#include <stdint.h>

typedef enum TextType_e
{
   TE_ERROR,
   TE_WARNING,
   TE_DEBUG
}TextType_t;

class IVkApiCallbacks
{
protected:
   ~IVkApiCallbacks()
   {
   }
public:
   virtual void OnApiConnected() = 0;
   virtual void OnRequestAppID() = 0;
   virtual void OnRequestAppKey() = 0;
   virtual void OnRequestAppServiceKey() = 0;
   virtual bool OnRecvTextMessage(TextType_t type, const char *sMessage)=0;
   virtual bool OnCaptchaRequest(uint64_t sID, const void *pData, size_t DataSz) = 0;
};

class IVKApi
{
protected:
   virtual ~IVKApi()
   { }
public:
   virtual void Free() = 0;
   virtual void Start()=0;
    
   virtual void SetConfigFilePath(const char *szFilePath)=0;
   virtual void SetAppID(uint32_t nID)=0;
   virtual void SetAppKey(const char* sKey)=0;
   virtual void SetAppServiceKey(const char* sKey)=0;
    
   virtual void SetUserToken(const char* sToken)=0;
   virtual void SetGAuthSecret(const char* sGAuthSecret)=0;
    
   virtual bool AddPermissionBit(const char* sName, uint32_t nValue)=0;
   virtual bool AddPermissionFlag(const char* sName)=0;
   virtual void SetApiVersion(const char *szApiVersion)=0;
   virtual void SetUserName(const char *UserName)=0;
   virtual void SetUserPassword(const char *Password)=0;
   virtual void SetCallbackHandler(IVkApiCallbacks *pCallbackHandler)=0;

   virtual void RunCallbacks() = 0;
   virtual void SetCaptchaText(const char *szText) = 0;
};

IVKApi *CreateVkApi();
#endif // IVkApi_h__
