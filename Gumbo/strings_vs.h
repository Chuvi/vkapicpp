/*Dummy file to satisfy source file dependencies on Windows platform*/
#ifdef WIN32
#define strcasecmp _stricmp
#define strncasecmp _strnicmp
#define inline __inline
#else
#include <strings.h>
#endif
