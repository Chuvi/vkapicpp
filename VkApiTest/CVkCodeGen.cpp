#include "stdafx.h"
#include "CVkCodeGen.h"
#include <fstream>
#include <sstream>
#include <iomanip>
#include <direct.h>
#include "..\VKApi\CJsonTools.h"


CVkCodeGen::CVkCodeGen()
{

}

CVkCodeGen::~CVkCodeGen()
{

}

bool CVkCodeGen::DoThis()
{
   if (!ParseMethods())
   {
      return false;
   }
   MakeCode();
   return true;
}

void CVkCodeGen::ParseObjects()
{
   const IVkSchema *Sh = GetVKSchema();
   if (!Sh->IsInitialised())
   {
      return;
   }
   auto Doc = Sh->GetDocument("objects.json");
   auto &Objects = (*Doc)["definitions"];
   if (!Objects.IsObject())
   {
      return;
   }
  
   for (auto &obj:Objects.GetObject())
   {
      ParseObject(obj.name.GetString(), obj.value);
   }
}

void CVkCodeGen::MakeCode()
{

   int nMaxParams = 0;
   std::string MaxFunc;
   _mkdir("..\\GeneratedCode");

   std::ofstream fOut;
   std::string sOutName;
   auto GetParamTypeStringComment = [](ParamType_t Type, bool IsArray)->std::string
   {
      std::string sType;
      if (IsArray)
      {
         sType.append("array of ");
      }
      switch (Type)
      {
         default:
         {
            sType.append("unknown");
            break;
         }
         case IT_BOOL:
            sType.append("boolean");
            break;
         case IT_INT:
            sType.append("integer");
            break;
         case IT_FLOAT:
            sType.append("number");
            break;
         case IT_STRING:
            sType.append("string");
            break;
      }
      if (IsArray)
      {
         sType.append("s");
      }
      return sType;
   };


   auto GetParamTypeString = [](ParamType_t Type, const std::string &sParamName, bool IsArray)->std::string
   {
      std::stringstream stOut;
      std::string sType;
      switch (Type)
      {
         default:
         {
            if (IsArray)
            {
               stOut << "const void **p" << sParamName << ", size_t n" << sParamName;
            }
            else
            {
               stOut << "const void *p" << sParamName;
            }
            break;
         }
         case IT_BOOL:
            if (IsArray)
            {
               stOut << "const bool *pb" << sParamName << ", size_t n" << sParamName;
            }
            else
            {
               stOut << "bool b" << sParamName;
            }
            break;
         case IT_INT:
            if (IsArray)
            {
               stOut << "const int *pi" << sParamName << ", size_t n" << sParamName;
            }
            else
            {
               stOut << "int i" << sParamName;
            }
            break;
         case IT_FLOAT:
            if (IsArray)
            {
               stOut << "const float *pf" << sParamName << ", size_t n" << sParamName;
            }
            else
            {
               stOut << "float f" << sParamName;
            }
            break;
         case IT_STRING:
            if (IsArray)
            {
               stOut << "const char **psz" << sParamName << ", size_t n" << sParamName;
            }
            else
            {
               stOut << "const char *sz" << sParamName;
            }
            break;
      }
      sType = stOut.str();
      return sType;
   };
   

   auto PrintParamsComment = [&fOut, GetParamTypeStringComment](const std::string &sParamsName, const std::vector<VKMethodParam_t> &vParams)
   {
      if (!vParams.empty())
      {
         fOut << "\t"<<sParamsName<<":\n";
         for (auto P : vParams)
         {
            fOut << "\t\t" << P.sName << ": " << GetParamTypeStringComment(P.iParamType, P.bIsArray);
            if (!P.sDescription.empty())
            {
               fOut << "(" << P.sDescription << ")";
            }
            fOut << "\n";
         }
         //fOut << "\n";
      }
   };

   auto PrintParamsSet = [&fOut, GetParamTypeString](const std::string &sParamsName, const std::vector<VKMethodParam_t> &vParams)
   {
      
      if (!vParams.empty())
      {
         //fOut << "\t" << sParamsName << ":\n";
         for (auto P : vParams)
         {
           
            fOut << "\tvirtual void Set" << P.sFixedName << "(" << GetParamTypeString(P.iParamType, P.sFixedName, P.bIsArray) << ")=0;\n";
         }
         //fOut << "\n";
      }
   };
   std::string sClassName;
   for (auto G : m_VkMethods)
   {
      sOutName.clear();
      sOutName.append("..\\GeneratedCode\\");
      sOutName.append("I");
      sOutName.append(G.first);
      sOutName.append(".h");
      fOut.open(sOutName);
      if (!fOut.is_open())
      {
         return;
      }
      fOut <<"//"<< G.first << std::endl;
      for (auto F : G.second)
      {
         fOut << "/*\n\t" << F.sName;
         if (!F.sDescription.empty())
         {
            fOut << "(" << F.sDescription << ")";
         }
         fOut << "\n";
         PrintParamsComment("Requered params", F.vRequeredParams);
         PrintParamsComment("Common params", F.vParams);
         fOut << "*/\n";

         fOut << "class I" << F.sGroupName << F.sFuncName << ":public ISomeBaseClass\n{\n\tpublic:\n\tI" << F.sGroupName << F.sFuncName << "(){}\n\tvirtual ~I" << F.sGroupName << F.sFuncName << "(){}\n";
         PrintParamsSet("Requered params", F.vRequeredParams);
         PrintParamsSet("Common params", F.vParams);
         fOut << "};\n";

         if (nMaxParams < (F.vRequeredParams.size() + F.vParams.size()))
         {
            nMaxParams = F.vRequeredParams.size() + F.vParams.size();
            MaxFunc = F.sName;
         }
      }
      fOut.close();
   }
   

   printf("Max=%i (%s)\n", nMaxParams, MaxFunc.c_str());
}

bool CVkCodeGen::ParseMethods()
{
   const IVkSchema *Sh = GetVKSchema();
   if (!Sh->IsInitialised())
   {
      return false;
   }
 
   auto Doc = Sh->GetDocument("methods.json");
   auto &methods = (*Doc)["methods"];
   if (!methods.IsArray())
   {
      return false;
   }
   
   for (auto method = methods.Begin(); method < methods.End(); ++method)
   {
      ParseMethod(method);

     /* if (method->HasMember("parameters"))
      {
         auto &Params = (*method)["parameters"];
         for (auto param = Params.Begin(); param < Params.End(); ++param)
         {
            //printf("%s\n", (*param)["name"].GetString());
         }
      }
      else
      {
         printf("%s\n", (*method)["name"].GetString());
      }*/
   }

   return true;
}

bool CVkCodeGen::ParseMethod(const rapidjson::Value *pMethod)
{
   VKMethod_t Method;
   Method.nTokenType = AT_NONE;
   
   const rapidjson::Value *pTokenType,*pParams;
   const char *szTokenType;
   if (!pMethod->IsObject())
   {
      return false;
   }
   if (!pMethod->HasMember("name"))
   {
      printf("Found method without name.\n");
      return false;
   }
   Method.sName = (*pMethod)["name"].GetString();
   auto DotPos = Method.sName.find('.');
   if (DotPos == Method.sName.npos)
   {
      printf("Method %s have not dot.\n", Method.sName.c_str());
      return false;
   }
   Method.sGroupName=Method.sName.substr(0, DotPos);
   Method.sFuncName = Method.sName.substr(DotPos+1, Method.sName.length());
   Method.sGroupName[0] = toupper(Method.sGroupName[0]);
   Method.sFuncName[0] = toupper(Method.sFuncName[0]);
   if (pMethod->HasMember("description"))
   {
      if ((*pMethod)["description"].IsString())
      {
         Method.sDescription = (*pMethod)["description"].GetString();
      }
      else
      {
         printf("%s description is not string\n", Method.sName.c_str());
         return false;
      }
   }

   if (pMethod->HasMember("access_token_type"))
   {
      pTokenType = &(*pMethod)["access_token_type"];
      if (pTokenType->IsArray())
      {
         for (auto TokenType = pTokenType->Begin(); TokenType < pTokenType->End(); ++TokenType)
         {
            if (TokenType->IsString())
            {
               szTokenType=TokenType->GetString();
               if (!strcmp(szTokenType, "open"))
               {
                  Method.nTokenType += AT_OPEN;
               }
               else if (!strcmp(szTokenType, "user"))
               {
                  Method.nTokenType += AT_USER;
               }
               else if (!strcmp(szTokenType, "group"))
               {
                  Method.nTokenType += AT_GROUP;
               }
               else if (!strcmp(szTokenType, "service"))
               {
                  Method.nTokenType += AT_SERVICE;
               }
               else
               {
                  printf("Unknown access type %s in %s\n", szTokenType,Method.sName.c_str());
                  return false;
               }
            }
            else
            {
               printf("%s access_token_type element is not string\n", Method.sName.c_str());
               return false;
            }
         }
      }
      else
      {
         printf("%s access_token_type is not array\n", Method.sName.c_str());
         return false;
      }
   }
   
  
   if (pMethod->HasMember("parameters"))
   {
     
      pParams = &(*pMethod)["parameters"];
      if (pParams->IsArray())
      {
         for (auto param = pParams->Begin(); param < pParams->End(); ++param)
         {
            if (!ParseParam(param, Method))
            {
               printf("Failed to parse %s params\n", Method.sName.c_str());
               return false;
            }
         }
      }
      else
      {
         printf("%s parameters is not array\n", Method.sName.c_str());
         return false;
      }
   }

   m_VkMethods[Method.sGroupName].push_back(Method);
   return true;
}

bool CVkCodeGen::ParseParam(const rapidjson::Value *pParam, VKMethod_t &rMethod)
{
   bool bParamRequered = false;

   std::string sFullName;
   VKMethodParam_t Param;
   auto ParseParamType = [&Param, &sFullName](bool bIsArray, const char *szName,float Min,float Max)->bool
   {
      if (Min != 0.0 || Max != 0.0)
      {
         printf("%s\n", szName);
      }
      if (!strcmp(szName, "boolean"))
      {
         Param.iParamType = IT_BOOL;
      }
      else if(!strcmp(szName, "integer"))
      {
         Param.iParamType = IT_INT;
      }
      else if (!strcmp(szName, "number"))
      {
         Param.iParamType = IT_FLOAT;
      }
      else if (!strcmp(szName, "string"))
      {
         Param.iParamType = IT_STRING;
      }
      else
      {
         printf("Unknown param type %s in %s\n", szName, sFullName.c_str());
         return false;
      }
      Param.bIsArray = bIsArray;
      return true;
   };

  

   if (!pParam)
   {
      printf("pParam==0");
      return false;
   }
   if (!pParam->IsObject())
   {
      printf("pParam is not object");
      return false;
   }
   const char *sParamType;

  
   bParamRequered = false;
   float flItemMin = 0.0, flItemMax = 0.0;
   if (!pParam->HasMember("name"))
   {
      printf("Found param without name in %s.\n", rMethod.sName.c_str());
      return false;
   }
   else
   {
      if (!(*pParam)["name"].IsString())
      {
         printf("Param name in %s is not string\n", rMethod.sName.c_str());
         return false;
      }
      Param.sName = (*pParam)["name"].GetString();
      Param.sFixedName = ToCamelCase(Param.sName);
   }
   sFullName = rMethod.sName + "." + Param.sName;
   if (!pParam->HasMember("type"))
   {
      printf("Param %s have no type\n", sFullName.c_str());
      return false;
   }
   if (!(*pParam)["type"].IsString())
   {
      printf("Param %s type is not string\n", sFullName.c_str());
      return false;
   }
   else
   {
      sParamType = (*pParam)["type"].GetString();
      if (!strcmp(sParamType, "array"))
      {
         if (!pParam->HasMember("items"))
         {
            printf("Param %s  type is array  without items\n", sFullName.c_str());
            return false;
         }
         if (!(*pParam)["items"].IsObject())
         {
            printf("Param %s  items is not object\n", sFullName.c_str());
            return false;
         }
         if (!(*pParam)["items"].HasMember("type"))
         {
            printf("Param %s  items has no type\n", sFullName.c_str());
            return false;
         }
         if (!(*pParam)["items"]["type"].IsString())
         {
            printf("Param %s  items type is not string\n", sFullName.c_str());
            return false;
         }
         if ((*pParam)["items"].HasMember("maximum"))
         {
            if (!(*pParam)["items"]["maximum"].IsNumber())
            {
               printf("Param %s  items.maximum is not Number\n", sFullName.c_str());
               return false;
            }
            flItemMax = (*pParam)["items"]["minimum"].GetFloat();
         }
         if ((*pParam)["items"].HasMember("maximum"))
         {
           
            if (!(*pParam)["items"]["minimum"].IsNumber())
            {
               printf("Param %s  items.minimum is not Number\n", sFullName.c_str());
               return false;
            }
            flItemMin = (*pParam)["items"]["minimum"].GetFloat();
         }

         if (!ParseParamType(true, (*pParam)["items"]["type"].GetString(), flItemMin, flItemMax))
         {
            return false;
         }
      }
      else
      {
         if (pParam->HasMember("items"))
         {
            printf("Param %s  type is not array  with items\n", sFullName.c_str());
            return false;
         }
         if (!ParseParamType(false, sParamType, 0.0, 0.0))
         {
            return false;
         }
      }
   }


   if (pParam->HasMember("required"))
   {
      if (!(*pParam)["required"].IsBool())
      {
         printf("Param %s required is not bool\n", sFullName.c_str());
         return false;
      }
      bParamRequered = (*pParam)["required"].GetBool();
   }
   if (pParam->HasMember("description"))
   {
      if ((*pParam)["description"].IsString())
      {
         Param.sDescription = (*pParam)["description"].GetString();
      }
      else
      {
         printf("Param %s description is not string\n", sFullName.c_str());
         return false;
      }
   }

   if (bParamRequered)
   {
      rMethod.vRequeredParams.push_back(Param);
   }
   else
   {
      rMethod.vParams.push_back(Param);
   }
   return true;
}


bool CVkCodeGen::ParseObject(const std::string &sName, const rapidjson::Value &pObj)
{
   
   if (!pObj.HasMember("type"))
   {
     
      return false;
   }

   std::string ObjType = pObj["type"].GetString();
   std::string NameSpace = GetNameSpace(sName);
   std::string ObjName = ToCamelCase(sName, true);

 
   if (pObj.HasMember("properties"))
   {
      printf("%s %s\n", GetNameSpace(sName).c_str(), ToCamelCase(sName, true).c_str());
      ParseObjectProperties(pObj["properties"]);
   }
   return true;
}

std::vector<VKObjectProperty_t> CVkCodeGen::ParseObjectProperties(const rapidjson::Value &pProps)
{
   std::vector<VKObjectProperty_t> vProps;
   VKObjectProperty_t Prop;
   std::string sPropType;
   if (pProps.IsObject())
   {
      for (auto &p : pProps.GetObject())
      {
         Prop.sName = p.name.GetString();
         Prop.sDescription.clear();
         sPropType.clear();
         if (p.value.HasMember("description"))
         {
            Prop.sDescription = p.value["description"].GetString();
         }
         if (p.value.HasMember("type"))
         {
            if (p.value["type"].IsString())
            {
               sPropType = p.value["type"].GetString();
            }
            else  if (p.value["type"].IsArray())
            {
             //  sPropType = p.value["type"].GetString();
            }
            else
            {
               printf(" %s :type is not string\n",Prop.sName.c_str());
            }
          
         }
         if (p.value.HasMember("$ref"))
         {
            printf("$ref (%s) %s\n",sPropType.c_str(),p.value["$ref"].GetString());
         }

      }
   }
   return vProps;
}

std::string CVkCodeGen::ToCamelCase(std::string sName, bool bSkipFirst/*=false*/)
{
   std::string::size_type fPos = 0;
   std::string sTail;
   bool bFirst = true;
   sName[0] = toupper(sName[0]);
   do
   {
      fPos = sName.find('_');
      if (fPos == sName.npos)
      {
         break;
      }
      sTail = sName.substr(fPos + 1, sName.length());
      sTail[0] = toupper(sTail[0]);
      if (bFirst&&bSkipFirst)
      {
         sName = sTail;
      }
      else
      {
         sName = sName.substr(0, fPos) + sTail;
      }

      bFirst = false;
   }
   while (true);

   return sName;
}

std::string CVkCodeGen::GetNameSpace(std::string sName)
{
   sName[0] = toupper(sName[0]);
   auto fPos = sName.find('_');
   if (fPos != std::string::npos)
   {
      return sName.substr(0, fPos);
   }
   return "";
}
