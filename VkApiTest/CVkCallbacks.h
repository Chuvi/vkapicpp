#ifndef CVkCallbacks_h__
#define CVkCallbacks_h__
#include <interface/IVkApi.h>


class CVkCallbacks:public IVkApiCallbacks
{
public:
   CVkCallbacks(IVKApi *pVkApi)
   {
      m_pVkApi = pVkApi;
   }
   virtual ~CVkCallbacks()
   { }

   virtual void OnApiConnected() override;

   virtual void OnRequestAppID() override;

   virtual void OnRequestAppKey() override;

   virtual void OnRequestAppServiceKey() override;

   virtual bool OnRecvTextMessage(TextType_t type, const char *sMessage) override;


   virtual bool OnCaptchaRequest(uint64_t sID, const void *pData, size_t DataSz) override;

private:
   IVKApi *m_pVkApi;

};
#endif // CVkCallbacks_h__
