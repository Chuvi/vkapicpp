#include "stdafx.h"
#include "CVkCallbacks.h"
#include <iostream>
#include <sstream>



void CVkCallbacks::OnApiConnected()
{
  
}

void CVkCallbacks::OnRequestAppID()
{
   printf(__FUNCTION__"\n");
}

void CVkCallbacks::OnRequestAppKey()
{
   printf(__FUNCTION__"\n");
}

void CVkCallbacks::OnRequestAppServiceKey()
{
   printf(__FUNCTION__"\n");
}

bool CVkCallbacks::OnRecvTextMessage(TextType_t type, const char *sMessage)
{
   printf("%s\n", sMessage);
   return false;
}

bool CVkCallbacks::OnCaptchaRequest(uint64_t sID, const void *pData, size_t DataSz)
{
   printf("");
   std::string sCapVal;
   FILE *CapIMG = fopen("captcha.jpg", "wb");
   if (CapIMG)
   {
      fwrite(pData, DataSz, 1, CapIMG);
      fclose(CapIMG);
      std::cout << "enter captcha:";
      std::cin >> sCapVal;
      m_pVkApi->SetCaptchaText(sCapVal.c_str());
   }
   return true;
}

