// CurlTest.cpp: ���������� ����� ����� ��� ����������� ����������.
//

#include "stdafx.h"


#ifndef LINUX
#include <conio.h>
#endif

#include <thread>
#include <atomic>
#include <iostream>
#include "CVkCallbacks.h"
#include "VKSchemaTools.h"
#include "CVkCodeGen.h"

#include "CBPeople.h"


void UpdateSchema()
{
   auto sch = GetVKSchema();
   sch->MakeSchemasCodeFiles("VkApiSchema.tt");
   
}

void MakeSchemaCode()
{
   CVkCodeGen CG;
   CG.DoThis();
}




int main(void)
{



   TestBP();
   //UpdateSchema();
   return 0;
/*
   CVkCodeGen CG;
   CG.ParseObjects();
   return 0;*/
   IVKApi *vk=CreateVkApi();
   
   CVkCallbacks *VkHandler = new CVkCallbacks(vk);
   vk->SetCallbackHandler(VkHandler);
   vk->SetConfigFilePath("..\\VkCfg.json");
   vk->Start();
   char c;
   bool ShouldBreak = false;
   do 
   {
      vk->RunCallbacks();
     #ifndef LINUX
		//do something in Linux
      if(!_kbhit())
         continue;
      c = _getch();
      switch(c)
      {
      default:
         break;
      case 'q':
         ShouldBreak = true;
         break;
      case 'r':
         
         break;
      }
		#endif
   }
   while(!ShouldBreak);
   printf("Free\n");
   vk->Free();
   printf("Exit\n");
   delete VkHandler;
   return 0;
}
