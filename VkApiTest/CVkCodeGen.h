#ifndef CVkCodeGen_h__
#define CVkCodeGen_h__
#include <interface\IVkSchema.h>
#include <string>
#include <vector>
#include <map>

enum AccessTokenType
{
   AT_NONE = 0,
   AT_OPEN = 1,
   AT_USER=2,
   AT_GROUP=4,
   AT_SERVICE=8,
};
typedef enum ParamType_e
{
   IT_BOOL,
   IT_INT,
   IT_FLOAT,
   IT_STRING
}ParamType_t;

typedef struct VKMethodParam_s
{
   std::string sName;
   std::string sFixedName;
   std::string sDescription;
   ParamType_t iParamType;
   bool bIsArray;
}VKMethodParam_t;

typedef struct VKMethod_s
{
   std::string sName;
   std::string sGroupName;
   std::string sFuncName;
   std::string sDescription;
   uint32_t nTokenType;
   std::vector<VKMethodParam_t> vRequeredParams;
   std::vector<VKMethodParam_t> vParams;
}VKMethod_t;


typedef struct VKObjectProperty_s
{
   std::string sName, sDescription;
}VKObjectProperty_t;

typedef struct VKObject_s
{
   std::string sName, sFixedName;
   std::vector<VKObjectProperty_t> vProps;
};

class CVkCodeGen
{
public:
   CVkCodeGen();
   ~CVkCodeGen();
   bool DoThis();
   void ParseObjects();
private:
   void MakeCode();
   bool ParseMethods();
   bool ParseMethod(const rapidjson::Value *pMethod);
   bool ParseParam(const rapidjson::Value *pParams, VKMethod_t &rMehod);
   bool ParseObject(const std::string &sName, const rapidjson::Value &pObj);
   std::vector<VKObjectProperty_t> ParseObjectProperties(const rapidjson::Value &pProps);

   std::string ToCamelCase(std::string sName,bool bSkipFirst=false);
   std::string  GetNameSpace(std::string sName);;
private:
  std::map<std::string,std::vector<VKMethod_s>> m_VkMethods;
};
#endif // CVkCodeGen_h__
