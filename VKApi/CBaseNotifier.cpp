#include "stdafx.h"
#include "CBaseNotifier.h"
#include <stdarg.h>





CBaseNotifier::CBaseNotifier()
{

}

CBaseNotifier::~CBaseNotifier()
{

}

bool CBaseNotifier::PrintText(TextType_t type, const char *szFmt, ...)
{
   va_list marker;
   bool Result = true;
   char StrFormatted[0x4000];
   int FormattedLen;
   if(szFmt)
   {
      va_start(marker, szFmt);
#ifdef _WIN32
      FormattedLen = _vsnprintf(StrFormatted, sizeof(StrFormatted) - 1, szFmt, marker);
#elif POSIX
      FormattedLen = vsnprintf(StrFormatted, sizeof(StrFormatted) - 1, szFmt, marker);
#else
#error "define vsnprintf type."
#endif
      va_end(marker);
      if(FormattedLen > 0)
      {      
         CallbackFn(true, Result, &IVkApiCallbacks::OnRecvTextMessage, type,StrFormatted);
         WaitCallbacks();
      }
   }
   return Result;
}
