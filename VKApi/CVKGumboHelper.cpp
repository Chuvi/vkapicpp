#include "stdafx.h"
#include "CVKGumboHelper.h"
#include "url.h"
#include "CBaseNotifier.h"
#include <string.h>


std::vector<HTTPFormData_t> CGumboHelper::GumboSearchForm(GumboNode* node, int nLevel /*= 0*/)
{
   //printf("%s:%i\n", __FUNCTION__, nLevel);
   std::vector<HTTPFormData_t> vFoundForms;
   if(node->type != GUMBO_NODE_ELEMENT)
   {
      return vFoundForms;
   }

   GumboAttribute* aAction;
   GumboAttribute* aMethod;
   HTTPFormData_t FormData;
   if(node->v.element.tag == GUMBO_TAG_FORM &&
      (aAction = gumbo_get_attribute(&node->v.element.attributes, "action")))
   {

      FormData.sActionURL = aAction->value;
      if(aMethod = gumbo_get_attribute(&node->v.element.attributes, "method"))
      {
         if(strcmp(aMethod->value, "post") == 0)
         {
            FormData.bPost = true;
         }
         else
         {
            FormData.bPost = false;
         }
      }
      auto vFoundFields = GumboParseForm(node);
      if(!vFoundFields.empty())
      {
         FormData.mFields.clear();
         for(auto Field : vFoundFields)
         {
            FormData.mFields[Field.sName] = Field.sValue;
         }
         // FormData.mFields.insert(FormData.mFields.end(), vFoundFields.begin(), vFoundFields.end());

      }
      vFoundForms.push_back(FormData);

   }

   GumboVector* children = &node->v.element.children;
   for(unsigned int i = 0; i < children->length; ++i)
   {
      auto RecvForms = GumboSearchForm(static_cast<GumboNode*>(children->data[i]), nLevel++);
      if(!RecvForms.empty())
      {
         vFoundForms.insert(vFoundForms.end(), RecvForms.begin(), RecvForms.end());
      }
   }
   return vFoundForms;
}

std::vector<HTTPFormField_t> CGumboHelper::GumboParseForm(GumboNode* node, int nLevel /*= 0*/)
{
   //printf("%s:%i\n", __FUNCTION__, nLevel);
   std::vector<HTTPFormField_t> vFields;
   if(node->type != GUMBO_NODE_ELEMENT)
   {
      return vFields;
   }

   HTTPFormField_t Field;
   GumboNode *ChildNode;
   GumboAttribute *InputName, *InputValue;
   GumboVector* children = &node->v.element.children;
   for(unsigned int i = 0; i < children->length; ++i)
   {
      ChildNode = static_cast<GumboNode*>(children->data[i]);
      if(ChildNode->v.element.tag == GUMBO_TAG_INPUT)
      {
         InputName = gumbo_get_attribute(&ChildNode->v.element.attributes, "name");

         if(InputName)
         {
            Field.sName = InputName->value;

            InputValue = gumbo_get_attribute(&ChildNode->v.element.attributes, "value");
            if(InputValue)
            {
               Field.sValue = InputValue->value;
            }
            else
            {
               Field.sValue.clear();
            }
            vFields.push_back(Field);
         }

      }
      else
      {
         auto RecvFields = GumboParseForm(ChildNode, nLevel + 1);
         if(!RecvFields.empty())
         {
            vFields.insert(vFields.end(), RecvFields.begin(), RecvFields.end());
         }
      }
   }
   return vFields;
}


std::vector<HTTPFormData_t> CGumboHelper::GumboFindForms(const std::string sResponceData)
{
   std::vector<HTTPFormData_t> vFormData;
   if(sResponceData.empty())
   {
      return vFormData;
   }
   GumboOutput* output = gumbo_parse(sResponceData.c_str());
   auto vForms = GumboSearchForm(output->root);
   gumbo_destroy_output(&kGumboDefaultOptions, output);
   return vForms;
}


std::vector<std::string> CGumboHelper::GumboSearchCaptchaImg(GumboNode* node, int nLevel /*= 0*/)
{

   std::vector<std::string> vFoundCaptchas;
   if(node->type != GUMBO_NODE_ELEMENT)
   {
      return vFoundCaptchas;
   }

   GumboAttribute* aClass;
   GumboAttribute* aImgSrc;
   //GumboVector* vAttributes;
   if(node->v.element.tag == GUMBO_TAG_IMG)
   {
      /* for (unsigned int i = 0; i < node->v.element.attributes.length; ++i)
      {
      GumboAttribute* attr = reinterpret_cast<GumboAttribute*>(node->v.element.attributes.data[i]);
      if (attr)
      {
      printf("%s %s\n", attr->name, attr->value);
      }
      }*/
      if(aClass = gumbo_get_attribute(&node->v.element.attributes, "class"))
      {
         if(strcmp(aClass->value, "captcha_img") == 0)
         {
            if(aImgSrc = gumbo_get_attribute(&node->v.element.attributes, "src"))
            {
               vFoundCaptchas.push_back(aImgSrc->value);
            }

         }
      }
   }

   GumboVector* children = &node->v.element.children;
   for(unsigned int i = 0; i < children->length; ++i)
   {
      auto RecvCaptchas = GumboSearchCaptchaImg(static_cast<GumboNode*>(children->data[i]), nLevel++);
      if(!RecvCaptchas.empty())
      {
         vFoundCaptchas.insert(vFoundCaptchas.end(), RecvCaptchas.begin(), RecvCaptchas.end());
      }
   }
   return vFoundCaptchas;
}

std::string CGumboHelper::GumboFindCaptchaImage(const std::string sResponceData)
{
   std::string sCaptchaURL;
   Url url;
   std::vector<std::string> vFoundCaptchas;
   GumboOutput* output = gumbo_parse(sResponceData.c_str());
   vFoundCaptchas = GumboSearchCaptchaImg(output->root, 0);
   gumbo_destroy_output(&kGumboDefaultOptions, output);

   if(!vFoundCaptchas.empty())
   {
      /*if(vFoundCaptchas.size() > 1)
      {
         PrintText(TE_WARNING, "%i captchas found. WTF?!", vFoundCaptchas.size());
      }*/
      url = vFoundCaptchas[0];
      if(url.scheme().empty())
      {
         url.scheme("https");
      }
      if(url.host().empty())
      {
         url.host("m.vk.com");
      }
      sCaptchaURL = url.str();
   }

   return sCaptchaURL;
}
