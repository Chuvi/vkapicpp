#include "stdafx.h"
#include "CVkSchema.h"
#include "VkApiSchema.h"
#include <fstream>

#define DSchTPrint(...) printf(__VA_ARGS__)

CVkSchema g_VkSchema;

const IVkSchema *GetVKSchema()
{
   return &g_VkSchema;
}



CVkSchema::CVkSchema() :m_bInitOK(false)
{
   Init();
}

CVkSchema::~CVkSchema()
{
   for (auto &ref : m_vRefs)
   {
      if (ref.pValidator)
      {
         delete ref.pValidator;
         ref.pValidator = 0;
      }
      if (ref.pSchema)
      {
         delete ref.pSchema;
         ref.pSchema = 0;
      }
      if (ref.pDocument)
      {
         delete ref.pDocument;
         ref.pDocument = 0;
      }
   }
   m_vRefs.clear();
}

bool CVkSchema::IsInitialised() const
{
   return m_bInitOK;
}

bool CVkSchema::Init()
{
   m_bInitOK =
      AddDocument(&_VK_methods, "methods.json") &&
      AddDocument(&_VK_schema, "schema.json") &&
      AddDocument(&_VK_objects, "objects.json") &&
      AddDocument(&_VK_responses, "responses.json") &&

      MakeSchema("methods.json") &&
      MakeSchema("schema.json") &&
      MakeSchema("objects.json") &&
      MakeSchema("responses.json");
   return m_bInitOK;
}


bool CVkSchema::AddDocument(const FileData_t *fData, const std::string &sName)
{
   SchemaRefInfo_t Ref;
   SchemaRefInfo_t *pRef = 0;
   for (auto &Doc : m_vRefs)
   {
      if (Doc.sName == sName)
      {
         pRef= &Doc;
         break;
      }
   }
   if (pRef)
   {
      return true;
   }
   Ref.sName = sName;
   if (!fData)
   {
      return false;
   }
   auto vData = Code2Data(fData);
   if (vData.empty())
   {
      DSchTPrint("Code2Data failed: %s\n", sName.c_str());
      return false;
   }
   std::string sData(vData.begin(), vData.end());
   Ref.pDocument = new rapidjson::Document;
   Ref.pDocument->Parse(sData.c_str());
   if (Ref.pDocument->HasParseError())
   {
      DSchTPrint("%s has parse errors\n", sName.c_str());
      delete Ref.pDocument;
      return false;
   }

   Ref.pSchema = 0;
   Ref.pValidator = 0;

   m_vRefs.push_back(Ref);
   return true;
}

bool CVkSchema::MakeSchema(const std::string &sName)
{
   SchemaRefInfo_t *pRef = 0;
   for (auto &Doc : m_vRefs)
   {
      if (Doc.sName == sName)
      {
         pRef = &Doc;
         break;
      }
   }
   if (!pRef)
   {
      return false;
   }

   if (!pRef->pSchema)
   {
      pRef->pSchema = new rapidjson::SchemaDocument(*pRef->pDocument, this);
      pRef->pValidator = new rapidjson::SchemaValidator(*pRef->pSchema);
   }
   return pRef->pSchema!=0;
}

const rapidjson::Document * CVkSchema::GetDocument(const std::string &sName) const
{
   const SchemaRefInfo_t *pRef = FindRef(sName);
   if (!pRef)
   {
      return 0;
   }
   return pRef->pDocument;
}

const rapidjson::SchemaDocument * CVkSchema::GetSchema(const std::string &sName) const
{
   const SchemaRefInfo_t *pRef = FindRef(sName);

   if (!pRef)
   {
      return 0;
   }
   return pRef->pSchema;
}

const rapidjson::SchemaValidator * CVkSchema::GetValidator(const std::string &sName) const
{
   const SchemaRefInfo_t *pRef = FindRef(sName);
   if (!pRef)
   {
      return 0;
   }
   return pRef->pValidator;
}

const rapidjson::SchemaDocument* CVkSchema::GetRemoteDocument(const char* uri, rapidjson::SizeType length)
{
   std::string UrlCopy = uri;
   auto HashPos = UrlCopy.find("#");
   if (HashPos == UrlCopy.npos)
   {
      printf("Ref failed (%s)\n", uri);
      return 0;
   }
   std::string sName = UrlCopy.substr(0, HashPos);
   SchemaRefInfo_t *pRef = 0;
   for (auto &Doc : m_vRefs)
   {
      if (Doc.sName == sName)
      {
         pRef = &Doc;
         break;
      }
   }
   if (!pRef)
   {
      return 0;
   }
   if (!pRef->pSchema)
   {
      if (!MakeSchema(sName))
      {
         return 0;
      }
   }
   return pRef->pSchema;
}

const CVkSchema::SchemaRefInfo_t * CVkSchema::FindRef(const std::string &sName) const
{
   for (auto &Doc : m_vRefs)
   {
      if (Doc.sName == sName)
      {
         return &Doc;
      }
   }
   return 0;
}

bool CVkSchema::MakeSchemasCodeFiles(const std::string &sOutName) const
{
   std::string sFullCode, sCode;
   if (sOutName.empty())
   {
      return false;
   }
   sCode = File2Code("..\\vk-api-schema\\methods.json", "VK_methods");
   if (sCode.empty())
   {
      return false;
   }
   sFullCode += sCode;
   sCode = File2Code("..\\vk-api-schema\\objects.json", "VK_objects");
   if (sCode.empty())
   {
      return false;
   }
   sFullCode += sCode;
   sCode = File2Code("..\\vk-api-schema\\responses.json", "VK_responses");
   if (sCode.empty())
   {
      return false;
   }
   sFullCode += sCode;
   sCode = File2Code("..\\vk-api-schema\\schema.json", "VK_schema");
   if (sCode.empty())
   {
      return false;
   }
   sFullCode += sCode;

   std::ofstream fOut(sOutName, std::ios::binary);
   if (!fOut.is_open())
   {
      return false;
   }
   fOut << sFullCode;
   fOut.close();
   return true;
}




