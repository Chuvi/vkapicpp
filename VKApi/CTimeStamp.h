#ifndef CTimeStamp_h__
#define CTimeStamp_h__

#include <ctime>


class CTimeStamp
{
public:
   CTimeStamp() :m_nTimeStamp(std::time(nullptr))
   {

   }
   CTimeStamp(uint32_t nTimeStamp) :m_nTimeStamp(nTimeStamp)
   {

   }
   CTimeStamp(const CTimeStamp &s) :m_nTimeStamp(s.m_nTimeStamp)
   {

   }
   CTimeStamp &operator=(uint32_t nTimeStamp)
   {
      m_nTimeStamp = nTimeStamp;
      return *this;
   }

   bool operator!()
   {
      return !m_nTimeStamp;
   }
   bool operator ==(const CTimeStamp &s) const
   {
      return m_nTimeStamp == s.m_nTimeStamp;
   }
   bool operator !=(const CTimeStamp &s) const
   {
      return m_nTimeStamp != s.m_nTimeStamp;
   }
   
   bool operator >(const CTimeStamp &s) const
   {
      return m_nTimeStamp > s.m_nTimeStamp;
   }
   bool operator >=(const CTimeStamp &s) const
   {
      return m_nTimeStamp >= s.m_nTimeStamp;
   }
   bool operator <(const CTimeStamp &s) const
   {
      return m_nTimeStamp < s.m_nTimeStamp;
   }
   bool operator <=(const CTimeStamp &s) const
   {
      return m_nTimeStamp <= s.m_nTimeStamp;
   }
   std::string Date() const
   {
      struct tm * dt;
      char buffer[30];
      dt = localtime(&m_nTimeStamp);
      strftime(buffer, sizeof(buffer), "%y/%m/%d", dt);
      return std::string(buffer);
   }
   std::string Time() const
   {
      struct tm * dt;
      char buffer[30];
      dt = localtime(&m_nTimeStamp);
      strftime(buffer, sizeof(buffer), "%H:%M:%S", dt);
      return std::string(buffer);
   }

   std::string DateTime() const
   {
      return Date() + ' ' + Time();
   }
   std::string operator()() const
   {
      return DateTime();
   }
private:
   time_t m_nTimeStamp;
};

#if 0
inline void TimeStampTest()
{
   CTimeStamp st, tt;
   st = 1516319566;
   printf("st DateTime=%s\n", st.DateTime().c_str());
   printf("tt DateTime=%s\n", tt.DateTime().c_str());


   printf("DateTime=%s\n", CTimeStamp().DateTime().c_str());
#define EqCheck(eq) printf("\"%s\"=%s\n",#eq,((eq)?"TRUE":"FALSE"));
   EqCheck(st < tt);
   EqCheck(st <= tt);
   EqCheck(st > tt);
   EqCheck(st >= tt);
   EqCheck(st == tt);
   EqCheck(st != tt);

   EqCheck(tt < st);
   EqCheck(tt <= st);
   EqCheck(tt > st);
   EqCheck(tt >= st);
   EqCheck(tt == st);
   EqCheck(tt != st);

   EqCheck(st < st);
   EqCheck(st <= st);
   EqCheck(st > st);
   EqCheck(st >= st);
   EqCheck(st == st);
   EqCheck(st != st);

   EqCheck(tt < tt);
   EqCheck(tt <= tt);
   EqCheck(tt > tt);
   EqCheck(tt >= tt);
   EqCheck(tt == tt);
   EqCheck(tt != tt);
}
#endif
#endif // CTimeStamp_h__
