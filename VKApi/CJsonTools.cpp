#include "stdafx.h"
#include "CJsonTools.h"



/*
kNullType = 0,      //!< null
kFalseType = 1,     //!< false
kTrueType = 2,      //!< true
kObjectType = 3,    //!< object
kArrayType = 4,     //!< array
kStringType = 5,    //!< string
kNumberType = 6     //!< number
*/

void WriteIdent(std::string &sData, int nIdent)
{
   for (int i = 0; i < nIdent; i++)
   {
      sData.push_back('\t');
   }
}
void JSON_DumpNumber(const rapidjson::Value &v, std::string &sData, int nIdent)
{
   sData.push_back('\"');
   if (v.IsDouble())
   {
      sData.append(std::to_string(v.GetDouble()));
   }
   else if (v.IsFloat())
   {
      sData.append(std::to_string(v.GetFloat()));
   }
   else if (v.IsUint64())
   {
      sData.append(std::to_string(v.GetUint64()));
   }
   else if (v.IsInt64())
   {
      sData.append(std::to_string(v.GetInt64()));
   }
   else if (v.IsUint())
   {
      sData.append(std::to_string(v.GetUint()));
   }
   else if (v.IsInt())
   {
      sData.append(std::to_string(v.GetInt()));
   }
   else
   {
      sData.append("ERR_NUM");
   }
   sData.push_back('\"');
}

void JSON_DumpArray(const rapidjson::Value &v, std::string &sData, int nIdent)
{
   /*WriteIdent(sData, nIdent);
   sData.push_back('\"');
   sData.append(Name);
   sData.append("\"\r\n");*/
   sData.append("\r\n");
   WriteIdent(sData, nIdent);
   nIdent++;
   sData.append("{\r\n");
   for (auto &El : v.GetArray())
   {
      WriteIdent(sData, nIdent);
     /* sData.push_back('\"');
      sData.append(El.name.GetString());
      sData.append("\"=");*/
      switch (El.GetType())
      {
         case rapidjson::Type::kNullType:
         {
            sData.append("NULL");
            break;
         }
         case rapidjson::Type::kFalseType:
         {
            sData.append("False");
            break;
         }
         case rapidjson::Type::kTrueType:
         {
            sData.append("True");
            break;
         }
         case rapidjson::Type::kObjectType:
         {
            JSON_DumpObject(El, sData, nIdent);
            break;
         }
         case rapidjson::Type::kArrayType:
         {
            JSON_DumpArray(El, sData, nIdent);
            break;
         }
         case rapidjson::Type::kStringType:
         {
            sData.push_back('\"');
            sData.append(El.GetString());
            sData.push_back('\"');

            break;
         }
         case rapidjson::Type::kNumberType:
         {
            JSON_DumpNumber(El, sData, nIdent);
            break;
         }
         default:
         {
            break;
         }
      }
      sData.push_back('\r');
      sData.push_back('\n');
   }
   nIdent--;
   WriteIdent(sData, nIdent);
   sData.append("}");
}

void JSON_DumpObject(const rapidjson::Value &v, std::string &sData,int nIdent)
{
   /*WriteIdent(sData, nIdent);
   sData.push_back('\"');
   sData.append(Name);
   sData.append("\"\r\n");*/
   sData.append("\r\n");
   WriteIdent(sData, nIdent);
   nIdent++;
   sData.append("{\r\n");

   for (auto &El : v.GetObject())
   {
      WriteIdent(sData, nIdent);
      sData.push_back('\"');
      sData.append(El.name.GetString());
      sData.append("\"=");
      switch (El.value.GetType())
      {
         case rapidjson::Type::kNullType:
         {
            sData.append("NULL");
            break;
         }
         case rapidjson::Type::kFalseType:
         {
            sData.append("False");
            break;
         }
         case rapidjson::Type::kTrueType:
         {
            sData.append("True");
            break;
         }
         case rapidjson::Type::kObjectType:
         {
            JSON_DumpObject(El.value, sData, nIdent);
            break;
         }
         case rapidjson::Type::kArrayType:
         {
            JSON_DumpArray(El.value, sData, nIdent);
            break;
         }
         case rapidjson::Type::kStringType:
         {
            sData.push_back('\"');
            sData.append(El.value.GetString());
            sData.push_back('\"');
            
            break;
         }
         case rapidjson::Type::kNumberType:
         {
            JSON_DumpNumber(El.value, sData, nIdent);
            break;
         }
         default:
         {
            break;
         }
      }
      sData.push_back('\r');
      sData.push_back('\n');
   }
   nIdent--;
   WriteIdent(sData, nIdent);
   sData.append("}");
}
