#include "stdafx.h"
#include "CVkApiConfig.h"

#include <rapidjson/istreamwrapper.h>
#include <rapidjson/document.h>
#include <iostream>
#include <fstream>
#include <vector>
#include <string>
#include "CBaseNotifier.h"

#define  MutexProtected(funcs) do{m_CfgMutex.lock();funcs;m_CfgMutex.unlock();}while(0);
#define GetConfigItem(item)\
   decltype(item) Result;\
   MutexProtected(Result = item);\
   return Result;

#define CheckConfigItemString(item)\
   bool bSet=false;\
   MutexProtected(bSet = !item.empty());\
   return bSet;
CVkConfig::CVkConfig():m_AppID(0), m_AppPermissionBits(0)
{
   
}


CVkConfig::~CVkConfig()
{

}


bool CVkConfig::ParseVKCfg()
{
 
   if(!CheckConfigFilePath())
   {
      return false;
   }
   auto sFileName = GetConfigFilePath();

   std::ifstream fJIn(sFileName, std::ios::binary);
   if(!fJIn.is_open())
   {
      return false;
   }
   rapidjson::Document::ValueType* pValue;
   rapidjson::IStreamWrapper StreamWrapper(fJIn);
   rapidjson::Document d;

   auto JSONGetMember = [&d, this, sFileName](const std::string &sMember)->rapidjson::Document::ValueType*
   {
      std::vector<std::string> Tokens;
      std::string CurJSONPath;
      const char *str, *begin;
      rapidjson::Document::ValueType *pValue = nullptr;

      if(!sMember.empty())
      {
         str = sMember.c_str();

         do
         {
            begin = str;

            while(*str != '.' && *str)
               str++;

            Tokens.push_back(std::string(begin, str));
         }
         while(0 != *str++);
      }
      if(Tokens.empty())
      {
         return nullptr;
      }

      if(!d.HasMember(Tokens[0].c_str()))
      {
         PrintText(TE_WARNING, "Document(\"%s\") doesn't have member \"%s\".", sFileName.c_str(), Tokens[0].c_str());
         return nullptr;
      }
      pValue = &d[Tokens[0].c_str()];
      CurJSONPath.append(Tokens[0]);
      if(Tokens.size() > 1)
      {
         for(decltype(Tokens.size()) i = 1; i < Tokens.size(); i++)
         {
            if(pValue->IsObject())
            {
               if(pValue->HasMember(Tokens[i].c_str()))
               {
                  pValue = &((*pValue)[Tokens[i].c_str()]);
                  CurJSONPath.append(".");
                  CurJSONPath.append(Tokens[i]);
               }
               else
               {
                  PrintText(TE_WARNING, "Document(\"%s\") doesn't have member \"%s\" in \"%s\".", sFileName.c_str(), Tokens[i].c_str(), CurJSONPath.c_str());
                  return nullptr;
               }
            }
            else
            {
               PrintText(TE_WARNING, "\"%s\" is not an object in document(\"%s\").", CurJSONPath.c_str(), sFileName.c_str());
               return nullptr;
            }
         }
      }
      PrintText(TE_DEBUG, "\"%s\" found.", CurJSONPath.c_str());
      return pValue;
   };

   enum JSON_Types
   {
      JT_Bool,
      JT_Object,
      JT_Array,
      JT_Number,
      JT_Int,
      JT_Uint,
      JT_Int64,
      JT_Uint64,
      JT_Double,
      JT_String,
      JT_Float,
   };
   auto ValidateJSONType = [sFileName, JSONGetMember, this](const std::string &sMember, JSON_Types Type, bool ShouldExist)->rapidjson::Document::ValueType*
   {
      auto Element = JSONGetMember(sMember);
      bool bValidType = false;
      char *sTypeName = "Unknown";
      if(!Element)
      {
         if(ShouldExist)
         {
            PrintText(TE_ERROR, "%s(\"%s\"). \"%s\" should exist.", __FUNCTION__, sFileName.c_str(), sMember.c_str());
         }
         return nullptr;
      }
      switch(Type)
      {
      default:
         break;
      case JT_Bool:bValidType = Element->IsBool(); sTypeName = "Bool"; break;
      case JT_Object:bValidType = Element->IsObject(); sTypeName = "Object"; break;
      case JT_Array:bValidType = Element->IsArray(); sTypeName = "Array"; break;
      case JT_Number:bValidType = Element->IsNumber(); sTypeName = "Number"; break;
      case JT_Int:bValidType = Element->IsInt(); sTypeName = "Int"; break;
      case JT_Uint:bValidType = Element->IsUint(); sTypeName = "Uint"; break;
      case JT_Int64:bValidType = Element->IsInt64(); sTypeName = "Int64"; break;
      case JT_Uint64:bValidType = Element->IsUint64(); sTypeName = "Uint64"; break;
      case JT_Double:bValidType = Element->IsDouble(); sTypeName = "Double"; break;
      case JT_String:bValidType = Element->IsString(); sTypeName = "String"; break;
      case JT_Float:bValidType = Element->IsFloat(); sTypeName = "Float"; break;
      }
      if(!bValidType)
      {
         PrintText(ShouldExist ? TE_ERROR : TE_WARNING, "%s(\"%s\"). \"%s\" should be %s.", __FUNCTION__, sFileName.c_str(), sMember.c_str(), sTypeName);
         return nullptr;
      }
      return Element;
   };

#define ParseJSONMember(sMember,type,func,ShouldExist) {pValue=ValidateJSONType(sMember,JT_##type,ShouldExist);if(pValue){func(pValue->Get##type());}}


   d.ParseStream<rapidjson::kParseCommentsFlag>(StreamWrapper);
   if(d.HasParseError())
   {
      PrintText(TE_ERROR, "%s(\"%s\"). JSON parsing error.ErrorCode=%i. Offset=%i.", __FUNCTION__, sFileName.c_str(), d.GetParseError(), d.GetErrorOffset());

      return false;
   }

   if(d.HasMember("VKAppPermissionBits") && d["VKAppPermissionBits"].IsObject())
   {

      for(auto m = d["VKAppPermissionBits"].MemberBegin(); m < d["VKAppPermissionBits"].MemberEnd(); ++m)
      {
         if(m->value.IsUint())
         {
            AddPermissionBit(m->name.GetString(), m->value.GetUint());

         }
         else
         {
            PrintText(TE_WARNING, "%s(\"%s\"). \"VKAppPermissionBits\" should be UINT. Check field \"%s\".", __FUNCTION__, sFileName.c_str(), m->name.GetString());
         }
      }
   }
   else
   {
      PrintText(TE_ERROR, "%s(\"%s\"). App permissions bits not set.", __FUNCTION__, sFileName.c_str());
      return false;
   }

   ParseJSONMember("VkAppConfig.AppID", Uint, SetAppID, false);
   ParseJSONMember("VkAppConfig.AppSecretKey", String, SetAppKey, false);
   ParseJSONMember("VkAppConfig.AppServiceKey", String, SetAppServiceKey, false);
   ParseJSONMember("VkAppConfig.ApiVersion", String, SetApiVersion, false);
   pValue = JSONGetMember("VkAppConfig.AppPermissions");
   if(pValue->IsObject())
   {
      m_AppPermissionBits = 0;
      for(auto m = pValue->MemberBegin(); m < pValue->MemberEnd(); ++m)
      {
         if((m->value.IsBool() && m->value.IsTrue()) || (m->value.IsUint() && m->value.GetUint() != 0))
         {
            AddPermissionFlag(m->name.GetString());
         }
      }
   }
   ParseJSONMember("UserDataConfig.UserName", String, SetUserName, false);
   ParseJSONMember("UserDataConfig.Password", String, SetUserPassword, false);
   ParseJSONMember("UserDataConfig.GAuthToken", String, SetGAuthSecret, false);
   ParseJSONMember("UserDataConfig.UserToken", String, SetUserToken, false);
   return true;
}

void CVkConfig::SetConfigFilePath(const char *szFilePath)
{
   if(szFilePath)
   {
      MutexProtected(m_sConfigPath = szFilePath);
   }
}

void CVkConfig::SetAppID(uint32_t nID)
{
   MutexProtected(m_AppID = nID);
}

void CVkConfig::SetAppKey(const char* sKey)
{
   MutexProtected(m_sAppKey = sKey);
}

void CVkConfig::SetAppServiceKey(const char* sKey)
{
   MutexProtected(m_sAppServiceKey = sKey);
}

void CVkConfig::SetUserToken(const char* sToken)
{
   MutexProtected(m_sUserToken = sToken);
}

void CVkConfig::SetGAuthSecret(const char* sGAuthSecret)
{
   MutexProtected(m_sGoogleAuthSecret = sGAuthSecret);
}

bool CVkConfig::AddPermissionBit(const char* sName, uint32_t nValue)
{
   if((nValue & (nValue - 1)) != 0)
   {
      PrintText(TE_ERROR, "%s: Only one bit should be set in mask. Check \"%s\" value.", __FUNCTION__, sName);
      return false;
   }
   MutexProtected(m_AppPermissionsBits[sName] = nValue);
   return true;
}

bool CVkConfig::AddPermissionFlag(const char* sName)
{
   bool bResult = true;
   MutexProtected(
   if(m_AppPermissionsBits.find(sName) != m_AppPermissionsBits.end())
   {
      bResult = true;
      m_AppPermissionBits |= m_AppPermissionsBits[sName];
   }
   else
   {
      bResult = false;
   }
   );
   if(!bResult)
   {
      PrintText(TE_WARNING, "%s:Unknown permission \"%s\"\n", __FUNCTION__, sName);
   }
   return bResult;
}

void CVkConfig::SetApiVersion(const char *szApiVersion)
{
   MutexProtected(m_sApiVersion = szApiVersion);
}

void CVkConfig::SetUserName(const char *UserName)
{
   MutexProtected(m_sUserName = UserName);
}

void CVkConfig::SetUserPassword(const char *Password)
{
   MutexProtected(m_sUserPassword = Password);
}

std::string CVkConfig::GetConfigFilePath()
{
   GetConfigItem(m_sConfigPath);
}

uint32_t CVkConfig::GetAppID()
{
   GetConfigItem(m_AppID);
}

std::string CVkConfig::GetAppKey()
{
   GetConfigItem(m_sAppKey);
}

std::string CVkConfig::GetAppServiceKey()
{
   GetConfigItem(m_sAppServiceKey);
}

std::string CVkConfig::GetUserToken()
{
   GetConfigItem(m_sUserToken);
}

std::string CVkConfig::GetGAuthSecret()
{
   GetConfigItem(m_sGoogleAuthSecret);
}

std::string CVkConfig::GetApiVersion()
{
   GetConfigItem(m_sApiVersion);
}

std::string CVkConfig::GetUserName()
{
   GetConfigItem(m_sUserName);
}

std::string CVkConfig::GetUserPassword()
{
   GetConfigItem(m_sUserPassword);
}

bool CVkConfig::CheckConfigFilePath()
{
   CheckConfigItemString(m_sConfigPath);
}

bool CVkConfig::CheckAppID()
{
   bool bSet = false;
   MutexProtected(bSet = m_AppID != 0);
   return bSet;
}

bool CVkConfig::CheckAppKey()
{
   CheckConfigItemString(m_sAppKey);
}

bool CVkConfig::CheckAppServiceKey()
{
   CheckConfigItemString(m_sAppServiceKey);
}

bool CVkConfig::CheckUserToken()
{
   CheckConfigItemString(m_sUserToken);
}

bool CVkConfig::CheckGAuthSecret()
{
   CheckConfigItemString(m_sGoogleAuthSecret);
}

bool CVkConfig::CheckApiVersion()
{
   CheckConfigItemString(m_sApiVersion);
}

bool CVkConfig::CheckUserName()
{
   CheckConfigItemString(m_sUserName);
}

bool CVkConfig::CheckUserPassword()
{
   CheckConfigItemString(m_sUserPassword);
}
