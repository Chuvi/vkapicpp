#ifndef CCallbackHandler_h__
#define CCallbackHandler_h__
#include <interface/IVkApi.h>
#include <atomic>
#include <queue>
#include <mutex>
#include <thread>
#include <iostream>
#include <string>
class ICallbackFn
{
public:
   virtual ~ICallbackFn(){}

   virtual void Run() = 0;
   virtual bool IsExecuted() = 0;
};

template <typename TRetType, typename TBindType>class CCallbackFn:public ICallbackFn
{
public:
   CCallbackFn(TRetType pRet, TBindType BindFn):m_Func(BindFn), m_Result(pRet)
   {
      m_bExecuted = false;
   }
   virtual ~CCallbackFn() override{}
   operator ICallbackFn*()
   {
      return this;
   }
   virtual void Run() override
   {
      m_Result = m_Func(); m_bExecuted = true;
   }virtual bool IsExecuted() override
   {
      return m_bExecuted;
   }
private:
   TRetType m_Result;
   TBindType m_Func;
   std::atomic_bool m_bExecuted;
};

template<typename TBindType> class CCallbackFnVoid:public ICallbackFn
{
public:
   CCallbackFnVoid(TBindType BindFn):m_Func(BindFn)
   {
      m_bExecuted = false;
   }
   operator ICallbackFn*()
   {
      return this;
   }
   virtual ~CCallbackFnVoid() override{}
   virtual void Run() override
   {
      m_Func(); m_bExecuted = true;
   }virtual bool IsExecuted() override
   {
      return m_bExecuted;
   }
private:
   TBindType m_Func;
   std::atomic_bool m_bExecuted;
};





class CVkCallbackHandler: public IVKApi
{
public:
   CVkCallbackHandler();
   virtual ~CVkCallbackHandler();
  
   void RemoveCallbacks();
   void WaitCallbacks();
   virtual void RunCallbacks() override;
   virtual void SetCallbackHandler(IVkApiCallbacks *pCallbackHandler) override;
   bool IsInThreadFunc()
   {
      return m_VKThread.get_id() == std::this_thread::get_id();
   }

   void StopVKThread()
   {
      if (IsInThreadFunc())
      {
         return;
      }
      m_bTerminateThread = true;
      if (m_VKThread.joinable())
      {
         RunCallbacks();
         m_VKThread.join();
      }
   }
   bool IsThreadTerminating()
   {
      return m_bTerminateThread;
   }

   template <typename TFunc, typename TClass>
   void StartVKThread(TFunc pFunc, TClass pClass)
   {
      if (m_VKThread.joinable())
      {
         return;
      }
      m_bTerminateThread = false;

      m_VKThread = std::thread(pFunc, pClass);
   }

   template<typename _RetType, typename _Fn, typename... _Args>bool CallbackFn(bool bDelayedCall, _RetType &pResult, _Fn&& _Fx, _Args&&... _Ax)
   {
      if(!m_pCallbacks)
      {
         return false;
      }

      auto fwd = std::bind(std::ref(_Fx), m_pCallbacks, /*std::forward<_Args>*/(_Ax)...);
      if(bDelayedCall&&IsInThreadFunc())
      {
         m_CallBackMutex.lock();
         m_Callbacks.push(new CCallbackFn<_RetType&, decltype(fwd)>(std::ref(pResult), fwd));
         m_CallBackMutex.unlock();
      }
      else
      {
         pResult = fwd();
      }

      return true;
   }

   template<typename _Fn, typename... _Args>bool CallbackFnVoid(bool bDelayedCall, _Fn&& _Fx, _Args&&... _Ax)
   {
      if(!m_pCallbacks)
      {
         return false;
      }
      auto fwd = std::bind(std::ref(_Fx), m_pCallbacks, /*std::forward<_Args>*/(_Ax)...);
      if (bDelayedCall&&IsInThreadFunc())
      {
         m_CallBackMutex.lock();
         m_Callbacks.push(new CCallbackFnVoid<decltype(fwd)>(fwd));
         m_CallBackMutex.unlock();
      }
      else
      {
         fwd();
      }
      return true;
   }
  
private:
  
   std::mutex m_CallBackMutex;
   IVkApiCallbacks* m_pCallbacks;
   std::queue<ICallbackFn*> m_Callbacks;
   std::thread m_VKThread;
   std::atomic_bool m_bTerminateThread;
};
#endif // CCallbackHandler_h__
