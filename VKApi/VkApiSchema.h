#ifndef VkApiSchema_h__
#define VkApiSchema_h__
#include <stdint.h>
#include <FileToCode.h>

extern FileData_t _VK_methods;
extern FileData_t _VK_objects;
extern FileData_t _VK_responses;
extern FileData_t _VK_schema;
#endif // VkApiSchema_h__
