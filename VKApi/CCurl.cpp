#include "stdafx.h"
#include "CCurl.h"
#include <stdarg.h>
#include <thread>
#include <string>
std::atomic_int CCurl::m_NumCurls = std::atomic_int{ 0 };
CCurl::CCurl()
{
   if (!m_NumCurls++)
   {
      curl_global_init(CURL_GLOBAL_DEFAULT);
   }
  
   m_pCurl = curl_easy_init();
   //fprintf(stderr, "curl_easy_init %x %x\n", std::this_thread::get_id().hash(), m_pCurl);
   Init();
   SetTimeout(35000L);
}

CCurl::~CCurl()
{
   Cleanup();
   if (!--m_NumCurls)
   {
      curl_global_cleanup();
   }
   
}

void CCurl::Cleanup()
{
   if (m_pCurl)
   {
      curl_easy_cleanup(m_pCurl);
      m_pCurl = nullptr;
   }
}
void CCurl::Reset()
{
   curl_easy_reset(m_pCurl);
   curl_easy_setopt(m_pCurl, CURLOPT_COOKIELIST, "ALL");
   Init();
}

void CCurl::Init()
{
   CURLcode res;
   //CURLOPT_CAINFO;
   //curl_easy_setopt(m_pCurl, CURLOPT_VERBOSE, 1L);
   res = curl_easy_setopt(m_pCurl, CURLOPT_COOKIEFILE, ""); /* start cookie engine */
   if (res != CURLE_OK)
   {
      fprintf(stderr, "curl_easy_setopt(CURLOPT_COOKIEFILE) failed: %s\n", curl_easy_strerror(res));
      
   }
   //curl_easy_setopt(m_pCurl, CURLOPT_COOKIEJAR, "Cook.txt"); /* start cookie engine */
  // 
   
   res = curl_easy_setopt(m_pCurl, CURLOPT_WRITEFUNCTION, static_cast<CURL_WRITEFUNCTION_PTR>([](void *contents, size_t size, size_t nitems, void *userdata)->size_t
   {
      size_t realsize = size * nitems;
      //printf("%s:%i\n", "CURLOPT_WRITEFUNCTION", size*nitems);
      std::vector<char> *vUserData = reinterpret_cast<std::vector<char>*>(userdata);
      size_t OldSize = vUserData->size();     
      vUserData->insert(vUserData->end(), reinterpret_cast<char*>(contents), reinterpret_cast<char*>(reinterpret_cast<size_t>(contents)+(size*nitems)));
      return vUserData->size() - OldSize;
   }));

   if (res != CURLE_OK)
   {
      fprintf(stderr, "curl_easy_setopt(CURLOPT_WRITEFUNCTION) failed: %s\n", curl_easy_strerror(res));

   }

   res = curl_easy_setopt(m_pCurl, CURLOPT_HEADERFUNCTION, static_cast<CURL_WRITEFUNCTION_PTR>([](void *contents, size_t size, size_t nitems, void *userdata)->size_t
   {
      size_t realsize = size * nitems;
     // printf("%s:%i\n", "CURLOPT_HEADERFUNCTION", size*nitems);
      std::vector<char> *vUserData = reinterpret_cast<std::vector<char>*>(userdata);
      size_t OldSize = vUserData->size();
      vUserData->insert(vUserData->end(), reinterpret_cast<char*>(contents), reinterpret_cast<char*>(reinterpret_cast<size_t>(contents)+(size*nitems)));
      return vUserData->size() - OldSize;
   }));
   if (res != CURLE_OK)
   {
      fprintf(stderr, "curl_easy_setopt(CURLOPT_HEADERFUNCTION) failed: %s\n", curl_easy_strerror(res));

   }
   res = curl_easy_setopt(m_pCurl, CURLOPT_WRITEDATA, reinterpret_cast<void*>(&m_vCurlData));
   if (res != CURLE_OK)
   {
      fprintf(stderr, "curl_easy_setopt(CURLOPT_WRITEDATA) failed: %s\n", curl_easy_strerror(res));

   }
   res = curl_easy_setopt(m_pCurl, CURLOPT_HEADERDATA, reinterpret_cast<void*>(&m_vHeaderData));
   if (res != CURLE_OK)
   {
      fprintf(stderr, "curl_easy_setopt(CURLOPT_HEADERDATA) failed: %s\n", curl_easy_strerror(res));

   }
   res = curl_easy_setopt(m_pCurl, CURLOPT_FOLLOWLOCATION, 0);
   if (res != CURLE_OK)
   {
      fprintf(stderr, "curl_easy_setopt(CURLOPT_FOLLOWLOCATION) failed: %s\n", curl_easy_strerror(res));

   }
   //curl_easy_setopt(m_pCurl, CURLOPT_VERBOSE, 1);
  // curl_easy_setopt(m_pCurl, CURLOPT_PROXY, "socks5://127.0.0.1:9999");
   
}

void CCurl::SetFollowLocation(bool bFollow)
{
   /*CURLcode res;
   res=curl_easy_setopt(m_pCurl, CURLOPT_FOLLOWLOCATION, bFollow ? 1 : 0);
   if (res != CURLE_OK)
   {
      fprintf(stderr, "curl_easy_setopt(CURLOPT_FOLLOWLOCATION) failed: %s\n", curl_easy_strerror(res));

   }*/
   m_bFollowLocation = bFollow;
}

std::string  CCurl::SendQuery(const char *strUrl, ...)
{
   CURLcode res;
   va_list marker;
   char StrRequest[1024*2];
   int FormattedLen;
   std::string sPostString;
   std::string sRedirectUrl;  
   std::string sCurlData;
   

   std::lock_guard<std::mutex> lGuard(m_Mux);
   ResetQueryData();
  
   if (strUrl)
   {

      va_start(marker, strUrl);
#ifdef _WIN32
      FormattedLen = _vsnprintf(StrRequest, sizeof(StrRequest) - 1, strUrl, marker);
#elif POSIX
      FormattedLen = vsnprintf(StrRequest, sizeof(StrRequest) - 1, strUrl, marker);
#else
#error "define vsnprintf type."
#endif
      va_end(marker);


      if (FormattedLen > 0 && m_pCurl)
      {
        // 
        
         curl_easy_setopt(m_pCurl, CURLOPT_URL, StrRequest);        
         if (!m_PostFields.empty())
         {
            for (auto PF : m_PostFields)
            {
               if (!sPostString.empty())
               {
                  sPostString.append("&");
               }
               if (!PF.first.empty())
               {
                  sPostString.append(PF.first);
                  if (!PF.second.empty())
                  {
                     sPostString.append("=");
                     sPostString.append(UrlEncode(PF.second));
                  }
               }
            }
            res=curl_easy_setopt(m_pCurl, CURLOPT_POST, 1);
            if (res != CURLE_OK)
            {
               fprintf(stderr, "curl_easy_setopt(CURLOPT_POST) failed: %s\n", curl_easy_strerror(res));

            }
            res=curl_easy_setopt(m_pCurl, CURLOPT_POSTFIELDS, sPostString.c_str());
            if (res != CURLE_OK)
            {
               fprintf(stderr, "curl_easy_setopt(CURLOPT_POSTFIELDS) failed: %s\n", curl_easy_strerror(res));

            }
            ClearPostFields();
         }
         else
         {
            res=curl_easy_setopt(m_pCurl, CURLOPT_POST, 0);
            if (res != CURLE_OK)
            {
               fprintf(stderr, "curl_easy_setopt(CURLOPT_POST) failed: %s\n", curl_easy_strerror(res));

            }
         }
         //fprintf(stderr, "CurlPerformStart %x %x\n", std::this_thread::get_id().hash(), m_pCurl);
         do
         {
            sRedirectUrl.clear();
            res = curl_easy_perform(m_pCurl);
            //fprintf(stderr, "CurlPerformEnd %x\n", std::this_thread::get_id().hash());
            if (res != CURLE_OK)
            {
               fprintf(stderr, "%x: curl_easy_perform() failed: %s\n", std::this_thread::get_id().hash(), curl_easy_strerror(res));
               m_vCurlData.clear();
               break;
            }
            char *url = NULL;
            if ((curl_easy_getinfo(m_pCurl, CURLINFO_REDIRECT_URL, &url) == CURLE_OK) && url)
            {
               sRedirectUrl.clear();
               sRedirectUrl.append(url);
            }
            url = 0;
            if ((curl_easy_getinfo(m_pCurl, CURLINFO_EFFECTIVE_URL, &url) == CURLE_OK) && url)
            {
               m_sEffectiveURL.clear();
               m_sEffectiveURL.append(url);
            }

            if (!sRedirectUrl.empty())
            {
               m_vRedirectUrl.push_back(sRedirectUrl);
               m_vCurlData.clear();
               curl_easy_setopt(m_pCurl, CURLOPT_URL, sRedirectUrl.c_str());
            }
         }
         while (m_bFollowLocation&&!sRedirectUrl.empty());
      }
   }
   // (m_vCurlData.begin(), m_vCurlData.end());
   sCurlData.clear();
   for (auto v : m_vCurlData)
   {
      sCurlData.push_back(v);
   }
   FILE *WTF = fopen("WTF.txt", "wb");
   if (WTF)
   {
      fwrite(m_vCurlData.data(), m_vCurlData.size(), 1, WTF);
      fclose(WTF);
   }
   if (!m_vCurlData.empty() && sCurlData.empty())
   {
      printf("");
   }
   return sCurlData;
}


size_t CCurl::AddPostField(const std::string &name, const std::string &value /*= nullptr*/)
{

   m_PostFields[name] = value;
   return m_PostFields.size();
}

size_t CCurl::AddPostFields(const std::map<std::string, std::string> &pFields)
{
   for (auto f : pFields)
   {
      AddPostField(f.first, f.second);
   }
   return m_PostFields.size();
}

void CCurl::ClearPostFields()
{
   m_PostFields.clear();
}

std::string CCurl::GetRedirectUrl()
{
   if (m_vRedirectUrl.empty())
   {
      return std::string();
   }
   else
   {
      return m_vRedirectUrl.back();
   }

}

bool CCurl::HasRedirect()
{
   return !(m_vRedirectUrl.empty());
}

std::string CCurl::UrlEncode(const std::string &pURL)
{
   std::string Result;
   char *pString = nullptr;
   if(m_pCurl)
   {
      pString = curl_easy_escape(m_pCurl, pURL.c_str(), 0);
      if(pString)
      {
         Result.append(pString);
         curl_free(pString);
      }
   }
   return Result;
}

std::string CCurl::UrlDecode(const std::string &pURL)
{
   std::string Result;
   char *pString = nullptr;
   int nOutLen = 0;
   if(m_pCurl)
   {
      pString = curl_easy_unescape(m_pCurl, pURL.c_str(), 0, &nOutLen);
      if(pString)
      {
         Result.append(pString);
         curl_free(pString);
      }
   }
   return Result;
}

std::string CCurl::GetEffectiveURI()
{
   return m_sEffectiveURL;
}

std::string CCurl::GetHeader()
{
   return std::string(m_vHeaderData.begin(), m_vHeaderData.end());
}

time_t CCurl::GetHeaderTime()
{
   if(m_vHeaderData.empty())
   {
      return 0;
   }
   time_t time = 0;
   std::string sHeader = GetHeader();
   const char DateStartMark[] = "Date: ";
   const char DateEndMark[] = "\r\n";
   size_t DateStartPos = sHeader.find(DateStartMark);
   size_t DateEndPos;
   std::string DateString;
   if(DateStartPos != sHeader.npos)
   {

      DateEndPos = sHeader.find(DateEndMark, DateStartPos);
      if(DateEndPos != sHeader.npos)
      {
         DateStartPos += sizeof(DateStartMark) - 1;
         //DateEndPos -= sizeof(DateEndMark);
         DateString.append(sHeader.substr(DateStartPos, DateEndPos - DateStartPos));
         time=curl_getdate(DateString.c_str(), 0);

      }
   }
   return time;
}

void CCurl::SetTimeout(uint32_t nTimeOut)
{
   curl_easy_setopt(m_pCurl, CURLOPT_TIMEOUT_MS, nTimeOut);

}

void CCurl::ResetQueryData()
{
   m_vRedirectUrl.clear();
   m_sEffectiveURL.clear();
   m_vCurlData.clear();
   m_vHeaderData.clear();
}



