
#ifndef CVKLP_h__
#define CVKLP_h__
#include <string>
#include <stdint.h>
#include <thread>
#include "CCurl.h"

class CVKApi;
class CVKLongPoll
{
public:
   typedef struct LongPollParams_s
   {
      std::string Key, Server;
      uint32_t ts, pts;
   }LongPollParams_t;
   CVKLongPoll(CVKApi *pOwnVKApi) :m_VkApi(pOwnVKApi), m_WaitTime(5)
   {}

   std::string GetURL() const
   {
      std::string sUrl = "https://" + m_Params.Server + "?act=a_check&key=" + m_Params.Key + "&ts=" + std::to_string(m_Params.ts) + "&wait=" + std::to_string(m_WaitTime) + "&mode=234&version=3";
      return sUrl;
   }

   void Start()
   {
      if (m_LPThread.joinable())
      {
         return;
      }
      m_bTerminateThread = false;

      m_LPThread = std::thread(&CVKLongPoll::LPThread, this);
   }

   void Terminate()
   {
      m_bTerminateThread = true;
      if (m_LPThread.joinable())
      {
         m_LPThread.join();
      }

   }
   void LPThread();
private:
   uint32_t m_WaitTime;
   LongPollParams_t m_Params;
   CCurl m_Curl;
   std::atomic_bool m_bTerminateThread;
   std::thread m_LPThread;
   CVKApi *m_VkApi;
};
#endif // CVKLP_h__
