#include "stdafx.h"
#include "CVKLP.h"
#include <fstream>
#include "CJsonTools.h"
#include "CVkApi.h"

void CVKLongPoll::LPThread()
{
   std::string sQueryResponce;

   std::ofstream fOut;

   rapidjson::Document d;
   std::string sText;
   do
   {
      if (m_Params.Key.empty() || m_Params.Server.empty() || m_Params.ts == 0)
      {
         m_Params = m_VkApi->GetLongPollServer();
         //m_Params.Key.pop_back();
      }
      else
      {
         sQueryResponce = m_Curl.SendQuery("https://%s?act=a_check&key=%s&ts=%u&wait=%u&mode=234&version=3", m_Params.Server.c_str(), m_Params.Key.c_str(), m_Params.ts, m_WaitTime);
         if (!sQueryResponce.empty())
         {
            d.Parse(sQueryResponce.c_str());

            if (d.HasMember("failed"))
            {
               if (d["failed"].IsUint())
               {
                  printf("failed %i\n", d["failed"].GetUint());
                  switch (d["failed"].GetUint())
                  {
                     case  1:
                     {
                        if (d.HasMember("ts") && d["ts"].IsUint())
                        {
                           m_Params.ts = d["ts"].GetUint();
                        }
                        else
                        {
                           m_Params.ts = 0;
                        }
                        break;
                     }
                     case 2:
                     case 3:
                     {
                        m_Params.Key.clear();
                        break;
                     }
                     default:
                        break;
                  }
               }
               continue;
            }

            if (d.HasMember("ts") && d["ts"].IsUint())
            {
               m_Params.ts = d["ts"].GetUint();
               // printf("New TS=%u\n", m_Params.ts);
            }
            if (d.HasMember("pts") && d["pts"].IsUint())
            {
               m_Params.pts = d["pts"].GetUint();
               // printf("New pts=%u\n", m_Params.pts);
            }

            std::string sUpdateText;
            if (d.HasMember("updates") && d["updates"].IsArray() && d["updates"].GetArray().Size() > 0)
            {
               sText.clear();
               fOut.open("LP_Log.txt", std::ios::binary | std::ios::app);
               if (fOut.is_open())
               {
                  JSON_DumpObject(/*"doc", */d.GetObject(), sText);
                  fOut << sText;
                  fOut.close();
               }

               printf("UPD\n");
            }
         }
      }


   }
   while (!m_bTerminateThread);
}



