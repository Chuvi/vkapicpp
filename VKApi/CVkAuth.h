#ifndef CVkAuth_h__
#define CVkAuth_h__

#include "CVkApiConfig.h"
#include "CCurl.h"
#include "CVKGumboHelper.h"

class CVKAuth :public CVkConfig
{
public:
   CVKAuth();
   ~CVKAuth();
   bool ReceiveUserToken();
   uint32_t GetUserID() const
   {
      return m_nUserID;
   }
   void SetUserID(uint32_t nID)
   {
      m_nUserID = nID;
   }
   virtual void SetCaptchaText(const char *szText) override;
   std::string RunCaptchaRequestCallback(uint64_t CaptchaSID, const std::string sCaptchaURL);
   CCurl &Curl()
   {
      return m_Curl;
   }
   bool IsValidResponce(const std::string &sResponce);
private:
   CCurl m_Curl;
   CGumboHelper m_Gumbo;
   uint32_t m_nUserID;
   std::string m_szCaptchaText;
};

#endif // CVkAuth_h__
