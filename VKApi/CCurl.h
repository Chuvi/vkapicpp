#ifndef CCurl_h__
#define CCurl_h__
#include <curl/curl.h>
#include <stdint.h>
#include <vector>
#include <string>
#include <atomic>
#include <mutex>
#include <map>


/*
typedef struct PostField_s
{
   std::string name;
   std::string value;
}PostField_t;*/

class CCurl
{

public:
   CCurl();
   ~CCurl();
   void Cleanup();
   void Reset();
   void Init();
   void SetFollowLocation(bool bFollow);
   std::string SendQuery(const char *strUrl, ...);
   size_t AddPostField(const std::string &name, const std::string &value = std::string());
   size_t AddPostFields(const std::map<std::string, std::string> &pFields);
   void ClearPostFields();
   std::string GetRedirectUrl();
   bool HasRedirect();
   std::string UrlDecode(const std::string &pURL);
private:
   std::string UrlEncode(const std::string &pURL);
   
public:
   std::string GetEffectiveURI();
   std::string GetHeader();
   time_t GetHeaderTime();
   void SetTimeout(uint32_t nTimeOut);
private:
   typedef size_t(*CURL_WRITEFUNCTION_PTR)(void*, size_t, size_t, void*);
   void ResetQueryData();
   std::map<std::string, std::string> m_PostFields;
   std::vector<std::string> m_vRedirectUrl;

   std::string m_sEffectiveURL;
   CURLM *m_pCurl;
   
   std::vector<char>  m_vCurlData;
   std::vector<char> m_vHeaderData;
   static std::atomic_int m_NumCurls;
   std::mutex m_Mux;
   bool m_bFollowLocation;
};

#endif // CCurl_h__
