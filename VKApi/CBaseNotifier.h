#ifndef CBaseNotifier_h__
#define CBaseNotifier_h__

#include "CCallbackHandler.h"

class CBaseNotifier:public CVkCallbackHandler
{
public:
   CBaseNotifier();
   virtual ~CBaseNotifier();
   bool PrintText(TextType_t type, const char *szFmt, ...);
};


#endif // CBaseNotifier_h__