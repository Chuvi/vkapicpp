#ifndef CVkSchema_h__
#define CVkSchema_h__
#include <interface/IVkSchema.h>

#include <FileToCode.h>


class CVkSchema : public rapidjson::IRemoteSchemaDocumentProvider,public IVkSchema
{
private:
   typedef struct SchemaRefInfo_s
   {

      std::string sName;
      rapidjson::Document *pDocument;
      rapidjson::SchemaDocument *pSchema;
      rapidjson::SchemaValidator *pValidator;
   }SchemaRefInfo_t;
public:
   CVkSchema();
   ~CVkSchema();
   virtual bool IsInitialised() const override;
   virtual const rapidjson::Document *GetDocument(const std::string &sName) const override;
   virtual const rapidjson::SchemaDocument *GetSchema(const std::string &sName)const override;
   virtual const rapidjson::SchemaValidator *GetValidator(const std::string &sName)const override;
   virtual const rapidjson::SchemaDocument* GetRemoteDocument(const char* uri, rapidjson::SizeType length) override;
   virtual bool MakeSchemasCodeFiles(const std::string &sOutName) const override;
private:
   bool Init();
   bool AddDocument(const FileData_t *fData, const std::string &sName);
   bool MakeSchema(const std::string &sName);
   const SchemaRefInfo_t *FindRef(const std::string &sName) const;
   std::vector<SchemaRefInfo_t> m_vRefs;
   bool m_bInitOK;
};




#endif // CVkSchema_h__