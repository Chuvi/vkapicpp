
#ifndef CVkApi_h__
#define CVkApi_h__

#include "CCurl.h"
#include <thread>
#include <atomic>

#include <type_traits>
#include <queue>
#include <mutex>
#include "CCallbackHandler.h"
#include <rapidjson/document.h>
#include "CTimeStamp.h"
#include "CVKLP.h"
#include "CVkAuth.h"


class CVKApi:public CVKAuth
{
public:
   CVKApi();
   ~CVKApi();
   virtual void Start() override;
   
   typedef struct AlbumInfo_s
   {
      int64_t id;
      int64_t   thumb_id;
      int64_t   owner_id;
      std::string   title;
      std::string   description;
      uint32_t   created;
      uint32_t   updated;
      uint32_t   size;
      uint32_t can_upload;

   }AlbumInfo_t;
   typedef struct PhotoInfo_s
   {
      int64_t id;
      int64_t   album_id;
      int64_t   owner_id;
      int64_t user_id;
      std::string   text;
      uint32_t   date;
   }PhotoInfo_t;

   typedef struct UserInfo_s
   {
      enum DeactivateType_e
      {
         DA_ACTIVE,
         DA_DELETED,
         DA_BANNED
      };
      //uint64_t id;
      std::string first_name, last_name;
      bool online;
      uint32_t Last_time;
      uint32_t last_patform;
      DeactivateType_e deactivated;
   }UserInfo_t;

   typedef struct ChatInfo_s
   {
      uint64_t admin_id;
      std::map<uint64_t, UserInfo_t> vUsers;
   }ChatInfo_t;
  
   std::vector<uint64_t> GetUserFriends(uint64_t nUserID=0);
   std::vector<AlbumInfo_t> GetGroupAlbums(int64_t nGroupID);
   PhotoInfo_t GetAlbumPhoto(const AlbumInfo_t &pAlbum, uint32_t nPhoto);
   std::map<uint64_t, UserInfo_t> GetGroupMembers(uint32_t nGroupID)
   {
      return GetGroupMembers(std::to_string(nGroupID));
   }
   std::map<uint64_t, UserInfo_t> GetGroupMembers(const std::string &sGroupID);
   bool GetUserInfo();
   bool JoinChat(uint32_t nChatID);
   bool SetChatTitle(uint32_t nChatID, const std::string &sTitle);
   bool LeaveChat(uint32_t nChatID);
  
   bool SendChatMessage(bool bChat, uint32_t nChatID, const std::string &sText);
   CVKLongPoll::LongPollParams_t GetLongPollServer();
   ChatInfo_t GetChatInfo(uint32_t nChatID);
   
  
private:
  
   std::map<uint64_t, UserInfo_t> ParseUsersArray(const rapidjson::Value &Users);
   std::string SendQuery(const std::string &sMethod, const std::map<std::string, std::string> &QueryData);
   bool CheckConfig(bool bRunCallbacks);
 
   
  
   void VkThreadFunc();

   void GetChatFriensCrossing();

   virtual void Free() override;
   int Rand(int nMin, int nMax);


private:
  
   //CVKLongPoll m_LP;
   std::chrono::system_clock::time_point m_LastQuery;
   
};


#endif // CVkApi_h__
