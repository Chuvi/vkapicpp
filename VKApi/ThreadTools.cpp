#include "stdafx.h"
#include "ThreadTools.h"
#include <thread>
#ifdef _LINUX
#include <pthread.h>
#endif


uint32_t ThreadGetCurrentId()
{
#ifdef _WIN32
   return GetCurrentThreadId();
#elif _LINUX
   return pthread_self();
#endif
}

void SetThreadName(uint32_t dwThreadID, const char *pszName)
{
#ifdef WIN32
   if(IsDebuggerPresent())
   {
      typedef struct tagTHREADNAME_INFO
      {
         DWORD dwType;        // must be 0x1000
         LPCSTR szName;       // pointer to name (in same addr space)
         DWORD dwThreadID;    // thread ID (-1 caller thread)
         DWORD dwFlags;       // reserved for future use, most be zero
      } THREADNAME_INFO;

      THREADNAME_INFO info;
      info.dwType = 0x1000;
      info.szName = pszName;
      info.dwThreadID = dwThreadID;
      info.dwFlags = 0;

      __try
      {
         const DWORD MS_VC_EXCEPTION = 0x406D1388;
         RaiseException(MS_VC_EXCEPTION, 0, sizeof(info) / sizeof(DWORD), (ULONG_PTR *)&info);
      }
      __except(EXCEPTION_CONTINUE_EXECUTION)
      {
      }
   }
#endif
}

void SetThreadName(const char* pFormat, ...)
{
#ifdef WIN32
   char szThreadName[200];
   szThreadName[0] = 0;
   va_list marker;
   va_start(marker, pFormat);
   _vsnprintf_s(szThreadName, sizeof(szThreadName) - 1, pFormat, marker);
   SetThreadName(GetCurrentThreadId(), szThreadName);
#endif
}

uint32_t g_MainThreadID = []()->uint32_t
{
   SetThreadName("MainThread (%i)", ThreadGetCurrentId());
   return ThreadGetCurrentId();
}();

bool ThreadInMainThread()
{
   return (ThreadGetCurrentId() == g_MainThreadID);
}

