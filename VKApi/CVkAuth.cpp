#include "stdafx.h"
#include "CVkAuth.h"
#include "GAuth.h"
#include "url.h"
#include "CVKGumboHelper.h"
#include <iomanip>
#include <inttypes.h>
#include "CJsonTools.h"
CVKAuth::CVKAuth():m_nUserID(0)
{

}

CVKAuth::~CVKAuth()
{

}

std::string g_sRespBack;
bool CVKAuth::ReceiveUserToken()
{


   std::string sQueryResponce, sRedirectURL;
   std::vector<HTTPFormData_t> vRecvForms;
   uint32_t GoogleAuthCode;
   HTTPFormData_t *pForm = nullptr;
   bool bLoginReentry = false;
   bool bGoogleAuthReentry = false;
   Url UrlInfo;
   std::string sTokenString, sTokenStringAsURL;
   std::string sCaptchaURL;
   auto FindForm = [](std::vector<HTTPFormData_t> &vForms, const char *RequestedField)->HTTPFormData_t*
   {
      for (auto &Form : vForms)
      {
         if (Form.mFields.find(RequestedField) != Form.mFields.end())
         {
            return &Form;
         }
      }return nullptr;
   };
   auto AddFormFields = [this](HTTPFormData_t *pForm)
   {
      if (pForm)
      {
         for (auto Field : pForm->mFields)
         {
            m_Curl.AddPostField(Field.first.c_str(), Field.second.c_str());
         }
      }
   };
   auto FixFormURL = [this](HTTPFormData_t *pForm)->std::string
   {
      if (!pForm || pForm->sActionURL.empty())
      {
         return "";
      }
      std::string sFixed = "";
      std::string sEffectiveURL;
      Url ActionURL(pForm->sActionURL), EffectiveURL;

      if (!ActionURL.host().empty())
      {
         sFixed = pForm->sActionURL;
      }
      else
      {
         sEffectiveURL = m_Curl.GetEffectiveURI();
         if (!sEffectiveURL.empty())
         {
            EffectiveURL = sEffectiveURL;
            if (!EffectiveURL.host().empty())
            {
               if (!EffectiveURL.scheme().empty())
               {
                  ActionURL.scheme(EffectiveURL.scheme());
               }

               ActionURL.host(EffectiveURL.host());
               if (!EffectiveURL.port().empty())
               {
                  ActionURL.port(EffectiveURL.port());
               }
               sFixed = ActionURL.str();
            }
         }
      }
      sFixed = m_Curl.UrlDecode(sFixed);

      return sFixed;
   };

   auto FindGrandAccessForm = [FixFormURL](std::vector<HTTPFormData_t> &vForms)->HTTPFormData_t*
   {
      for (auto &Form : vForms)
      {
         auto FixedURL = FixFormURL(&Form);
         if (!FixedURL.empty())
         {
            Url url(FixedURL);
            for (auto QueryValue : url.query())
            {
               if (QueryValue.key() == "act"&&QueryValue.val() == "grant_access")
               {
                  return &Form;
               }
            }
         }
      }
      return nullptr;
   };

   auto ProcessCaptchaRequest = [this](HTTPFormData_t *pForm, const std::string &sCaptchaURL)->bool
   {
      std::string CaptchaURL = sCaptchaURL;
      std::string sCaptchaText;
      uint64_t nCaptchaSID = 0;
      if (!pForm)
      {
         return false;
      }
      if (pForm->mFields.find("captcha_key") == pForm->mFields.end() || pForm->mFields.find("captcha_sid") == pForm->mFields.end())
      {
         return false;
      }
      if (CaptchaURL.empty())
      {
         CaptchaURL.append("https://m.vk.com/captcha.php?sid=");
         CaptchaURL.append(pForm->mFields["captcha_sid"]);
      }
      if (1 != sscanf(pForm->mFields["captcha_sid"].c_str(), "%"SCNu64, &nCaptchaSID))
      {
         nCaptchaSID = 0;
      }
      sCaptchaText = RunCaptchaRequestCallback(nCaptchaSID, CaptchaURL);
      if (sCaptchaText.empty())
      {
         return false;
      }
      pForm->mFields["captcha_key"] = sCaptchaText;
      return true;
   };

   if (m_AppID == 0)
   {
      PrintText(TE_ERROR, "%s: AppID not set\n", __FUNCTION__);
      return false;
   }
   sQueryResponce = m_Curl.SendQuery("https://oauth.vk.com/authorize?client_id=%i&display=mobile&redirect_uri=https://oauth.vk.com/blank.html&scope=%i&response_type=token", m_AppID, m_AppPermissionBits);
   g_sRespBack = sQueryResponce;
   bool bHaveForm = false;
   do
   {
      pForm = nullptr;
      if (!IsValidResponce(sQueryResponce))
      {
         break;
      }
      vRecvForms = m_Gumbo.GumboFindForms(sQueryResponce);
      sCaptchaURL = m_Gumbo.GumboFindCaptchaImage(sQueryResponce);
      if (nullptr!=(pForm = FindForm(vRecvForms, "email")))
      {
         PrintText(TE_DEBUG, "Process email form.");
         if (ProcessCaptchaRequest(pForm, sCaptchaURL))
         {
            bLoginReentry = false;
         }
         if (bLoginReentry)
         {
            PrintText(TE_ERROR, "Invalid UserName or password.");
            break;
         }
         pForm->mFields["email"] = /*m_Curl.UrlEncode*/(m_sUserName);
         pForm->mFields["pass"] = /*m_Curl.UrlEncode*/(m_sUserPassword);
         AddFormFields(pForm);
         m_Curl.SetFollowLocation(true);
         sQueryResponce = m_Curl.SendQuery(FixFormURL(pForm).c_str());
         g_sRespBack = sQueryResponce;
         bLoginReentry = true;
         continue;
      }
      if (nullptr != (pForm = FindForm(vRecvForms, "code")))
      {
         PrintText(TE_DEBUG, "Process GoogleAuth form.");
         if (ProcessCaptchaRequest(pForm, sCaptchaURL))
         {
            bGoogleAuthReentry = false;
         }
         if (bGoogleAuthReentry)
         {
            PrintText(TE_ERROR, "Invalid GoogleAuth code (Generated=%u).", GoogleAuthCode);
            break;
         }
         GoogleAuthCode = GenerateAuthCode(m_sGoogleAuthSecret.c_str());

         pForm->mFields["code"] = std::to_string(GoogleAuthCode);
         AddFormFields(pForm);
         m_Curl.SetFollowLocation(true);
         sQueryResponce = m_Curl.SendQuery(FixFormURL(pForm).c_str());
         g_sRespBack = sQueryResponce;
         bGoogleAuthReentry = true;
         continue;
      }

      if ((nullptr != (pForm = FindGrandAccessForm(vRecvForms))) || (!m_Curl.GetRedirectUrl().empty() && m_Curl.GetRedirectUrl().find("blank.html#access_token") != std::string::npos))
      {
         PrintText(TE_DEBUG, "Process GrantAccess form.");
         if (pForm)
         {
            m_Curl.SetFollowLocation(false);
            sQueryResponce = m_Curl.SendQuery(FixFormURL(pForm).c_str());
            g_sRespBack = sQueryResponce;
         }
         while (m_Curl.HasRedirect())
         {
            sRedirectURL = m_Curl.UrlDecode(m_Curl.GetRedirectUrl());
            UrlInfo = sRedirectURL;
            if (UrlInfo.host() == "oauth.vk.com"&&UrlInfo.path() == "/blank.html")
            {
               sTokenString = UrlInfo.fragment();
               break;
            }
            if (pForm)
            {
               sQueryResponce = m_Curl.SendQuery(sRedirectURL.c_str());
               g_sRespBack = sQueryResponce;
            }
            // printf("");
         }
         break;
      }

      printf("");
   }
   while (sTokenString.empty());


   if (sTokenString.empty())
   {
      PrintText(TE_ERROR, "Empty token string.");
      return false;
   }
   sTokenStringAsURL.clear();
   sTokenStringAsURL.append("/null?");
   sTokenStringAsURL.append(sTokenString);
   UrlInfo = sTokenStringAsURL;
   UrlInfo.host();

   for (auto TokenVal : UrlInfo.query())
   {
      printf("%s %s\n", TokenVal.key().c_str(), TokenVal.val().c_str());
      if (TokenVal.key() == "access_token")
      {
         m_sUserToken = TokenVal.val();
      }
   }

   if (!m_sUserToken.empty())
   {
      PrintText(TE_DEBUG, "Token received: %s", m_sUserToken.c_str());
      //m_Curl.AddPostField("access_token", m_sAppServiceKey.c_str());
      //m_Curl.AddPostField("v", m_sApiVersion.c_str());
      //m_Curl.AddPostField("token", m_sAppServiceKey.c_str());
      //sQueryResponce = m_Curl.SendQuery("https://api.vk.com/method/secure.checkToken");

      return true;
   }
   return false;

}



void CVKAuth::SetCaptchaText(const char *szText)
{
   m_szCaptchaText = szText;
}

std::string CVKAuth::RunCaptchaRequestCallback(uint64_t CaptchaSID, const std::string sCaptchaURL)
{
   std::string sCaptchaData;
   m_szCaptchaText.clear();
   bool Result = false;
   if (!sCaptchaURL.empty())
   {
      sCaptchaData = m_Curl.SendQuery("%s", sCaptchaURL.c_str());
      if (!sCaptchaData.empty())
      {
         CallbackFn(true, Result, &IVkApiCallbacks::OnCaptchaRequest, CaptchaSID, sCaptchaData.data(),sCaptchaData.length());
         WaitCallbacks();
      }
   }
 
   return m_szCaptchaText;
}


bool CVKAuth::IsValidResponce(const std::string &sResponce)
{
   std::string sError, sErrDescript;
   if (sResponce.empty())
   {
      PrintText(TE_WARNING, "Empty responce.");
      return false;
   }
   uint32_t nErrCode = 0;
   rapidjson::Document d;
   d.Parse(sResponce.c_str());
   if (d.HasParseError())
   {
      //Not a JSON string
      return true;
   }
   //{"error":"invalid_request","error_description":"application was deleted"}
   if (d.HasMember("error"))
   {
      if (d["error"].IsString())
      {
         sError.append(d["error"].GetString());
      }
      else
      {
         if (d["error"].IsObject())
         {
            if (d["error"].HasMember("error_code"))
            {
               nErrCode = d["error"]["error_code"].GetInt();
            }
            if (d["error"].HasMember("error_msg"))
            {
               sError.append(d["error"]["error_msg"].GetString());
            }
         }
      }

   }
   if (d.HasMember("error_description"))
   {
      sErrDescript.append(d["error_description"].GetString());
   }
   if (!sError.empty() || !sErrDescript.empty())
   {
      PrintText(TE_WARNING, "Error:%i %s (%s)", nErrCode, sError.c_str(), sErrDescript.c_str());
      return false;
   }
   return true;
}

