
#ifndef CVkApiConfig_h__
#define CVkApiConfig_h__
#include <interface/IVkApi.h>
#include <string>
#include <map>
#include "CCallbackHandler.h"
#include "CBaseNotifier.h"


class CVkConfig:public CBaseNotifier
{
public:
   CVkConfig();
   virtual ~CVkConfig();
   bool ParseVKCfg();
   virtual void SetConfigFilePath(const char *szFilePath) override;
   virtual void SetAppID(uint32_t nID) override;
   virtual void SetAppKey(const char* sKey) override;
   virtual void SetAppServiceKey(const char* sKey) override;

   virtual void SetUserToken(const char* sToken) override;
   virtual void SetGAuthSecret(const char* sGAuthSecret) override;

   virtual bool AddPermissionBit(const char* sName, uint32_t nValue) override;
   virtual bool AddPermissionFlag(const char* sName) override;
   virtual void SetApiVersion(const char *szApiVersion) override;
   virtual void SetUserName(const char *UserName) override;
   virtual void SetUserPassword(const char *Password) override;

   std::string GetConfigFilePath();
   uint32_t GetAppID();
   std::string GetAppKey();
   std::string GetAppServiceKey();
   std::string GetUserToken();
   std::string GetGAuthSecret();
   std::string GetApiVersion();
   std::string GetUserName();
   std::string GetUserPassword();

   bool CheckConfigFilePath();
   bool CheckAppID();
   bool CheckAppKey();
   bool CheckAppServiceKey();
   bool CheckUserToken();
   bool CheckGAuthSecret();
   bool CheckApiVersion();
   bool CheckUserName();
   bool CheckUserPassword();
  // private:
   uint32_t m_AppID;
   std::string m_sApiVersion;
   std::string m_sAppKey;
   std::string m_sAppServiceKey;
   std::string m_sUserToken;
   std::string m_sGoogleAuthSecret;
   std::string m_sUserName;
   std::string m_sUserPassword;
   std::string m_sConfigPath;
   uint32_t m_AppPermissionBits;
   std::map<std::string, uint32_t> m_AppPermissionsBits;
   std::mutex m_CfgMutex;
};
#endif // CVkApiConfig_h__
