#ifndef CJsonTools_h__
#define CJsonTools_h__
#include <string>
#include <rapidjson/istreamwrapper.h>
#include <rapidjson/document.h>

void JSON_DumpObject(/*const char *Name, */const rapidjson::Value &v, std::string &sData, int nIdent=0);
#endif // CJsonTools_h__
