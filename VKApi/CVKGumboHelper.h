#ifndef CVKGumboHelper_h__
#define CVKGumboHelper_h__
#include <gumbo.h>
#include <vector>
#include <string>
#include <map>
typedef struct HTTPFormField_s
{
   std::string sName;
   std::string sValue;
}HTTPFormField_t;

typedef struct HTTPFormData_s
{
   std::string sActionURL;
   bool bPost;
   //std::vector<HTTPFormField_t> mFields;
   std::map<std::string, std::string> mFields;
}HTTPFormData_t;
class CGumboHelper
{
public:
   std::vector<HTTPFormData_t> GumboSearchForm(GumboNode* node, int nLevel = 0);
   std::vector<HTTPFormField_t> GumboParseForm(GumboNode* node, int nLevel = 0);
   std::vector<HTTPFormData_t> GumboFindForms(const std::string sResponceData);
   std::vector<std::string> GumboSearchCaptchaImg(GumboNode* node, int nLevel /*= 0*/);
   std::string GumboFindCaptchaImage(const std::string sResponceData);
};
#endif // CVKGumboHelper_h__