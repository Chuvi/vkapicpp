#ifndef ThreadTools_h__
#define ThreadTools_h__
#include <stdint.h>

uint32_t ThreadGetCurrentId();
void SetThreadName(const char* pFormat, ...);
bool ThreadInMainThread();
#endif // ThreadTools_h__
