#include "stdafx.h"
#include "CCallbackHandler.h"
#include "ThreadTools.h"



CVkCallbackHandler::CVkCallbackHandler()
{
   m_bTerminateThread = false;
}

CVkCallbackHandler::~CVkCallbackHandler()
{

}


void CVkCallbackHandler::RemoveCallbacks()
{
   m_CallBackMutex.lock();
   while(!m_Callbacks.empty())
   {
      m_Callbacks.pop();
   }
   m_CallBackMutex.unlock();
}

void CVkCallbackHandler::WaitCallbacks()
{
   bool bEmpty = false;
   if (!IsInThreadFunc())
   {
      return;
   }
   if (IsThreadTerminating())
   {
      return;
   }

   do
   {
      m_CallBackMutex.lock();
      if(m_Callbacks.empty())
      {
         bEmpty = true;
      }
      m_CallBackMutex.unlock();
   }
   while(!bEmpty);
   
}

void CVkCallbackHandler::RunCallbacks()
{
  
   m_CallBackMutex.lock();
   while(!m_Callbacks.empty())
   {
      m_Callbacks.front()->Run();
      delete m_Callbacks.front();
      m_Callbacks.pop();
   }
   m_CallBackMutex.unlock();
}

void CVkCallbackHandler::SetCallbackHandler(IVkApiCallbacks *pCallbackHandler)
{
   m_pCallbacks = pCallbackHandler;
}