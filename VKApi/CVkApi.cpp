#include "stdafx.h"
#include "CVkApi.h"
#include <rapidjson/istreamwrapper.h>
#include <rapidjson/document.h>
#include <rapidjson/writer.h>
#include <rapidjson/ostreamwrapper.h>
#include <rapidjson/prettywriter.h>
#include <iostream>
#include <fstream>

#include <cctype>
#include <iomanip>
#include <sstream>
#include <regex>

#include "CBaseNotifier.h"
#include "ThreadTools.h"
#include <random>
#include "CJsonTools.h"



IVKApi *CreateVkApi()
{
   return new CVKApi;
}

CVKApi::CVKApi() :  m_LastQuery(std::chrono::system_clock::from_time_t(0))
{
   srand(time(0));
}



CVKApi::~CVKApi()
{
   
}

void CVKApi::Start()
{
   
   StartVKThread(&CVKApi::VkThreadFunc, this);

}


std::vector<uint64_t> CVKApi::GetUserFriends(uint64_t nUserID)
{
   if (!nUserID)
   {
      nUserID = GetUserID();
   }
   std::string sQueryResponce;
   std::vector<uint64_t> vFriends;
   if (m_sUserToken.empty())
   {
      return vFriends;
   }
   int nCount = 0;
   int nCurID = 0;
   std::map<std::string, std::string> QData;

   std::string Code;

   int nOldCount = 0;

   do
   {
      Code =
         "var Offset=" + std::to_string(vFriends.size()) + "; \n"\
         "var nNumUsers=1;\n"\
         "var NumApiCalls=0;\n"\
         "var CurUsers;\n"\
         "var Users=[];\n"\
         "while(NumApiCalls<25&&Users.length<nNumUsers)\n"\
         "{\n"\
         "CurUsers=API.friends.get({\"v\": \"" + m_sApiVersion + "\",\"user_id\":" + std::to_string(nUserID) + ",\"order\":\"hints\",\"offset\":Offset,\"count\":5000});\n"\
         "nNumUsers=CurUsers.count;\n"\
         "Users=Users+CurUsers.items;\n"\
         "Offset=Offset+5000;\n"\
         "NumApiCalls=NumApiCalls+1;\n"\
         "}\n"\
         "return {\"total\":nNumUsers,\"users\":Users};\n";
  
      QData["code"] = Code;

      sQueryResponce = SendQuery("execute", QData);
      if (!IsValidResponce(sQueryResponce))
      {
         printf("Invalid respounse: user \"%u\"\n",nUserID);
         vFriends.clear();
         return vFriends;
      }

      rapidjson::Document d;
      d.Parse(sQueryResponce.c_str());

      if (!d.HasMember("response"))
      {
         vFriends.clear();
         return vFriends;
      }
      auto &resp = d["response"];
      if (!resp.HasMember("total") || !resp.HasMember("users"))
      {
         vFriends.clear();
         return vFriends;
      }
      if (resp["total"].IsNull())
      {
         vFriends.clear();
         return vFriends;
      }
      nCount = resp["total"].GetInt();
      if (!resp["users"].IsArray())
      {
         vFriends.clear();
         return vFriends;
      }
      for (auto &u : resp["users"].GetArray())
      {
         vFriends.push_back(u.GetUint());
      }

      if (nCount)
      {
       //  printf("Received %i-%i friends from %i (user %i)\n", nOldCount, vFriends.size(), nCount, nUserID);
         nOldCount = vFriends.size();
      }

   }
   while (nCount != vFriends.size());

   return vFriends;

}




bool CVKApi::CheckConfig(bool bRunCallbacks)
{
   typedef struct VKCfgChecks_s
   {
      bool(*fnCheckFunc)();

   }VKCfgChecks_t;

   bool bHaveErrors = false;
   if(!CheckAppID())
   {
      if (bRunCallbacks)
      {
         CallbackFnVoid(true, &IVkApiCallbacks::OnRequestAppID);
      }
      else
      {
         PrintText(TE_ERROR, "AppID not set.");
         bHaveErrors = true;
      }
   }
   
   if(!CheckAppKey())
   {
      if (bRunCallbacks)
      {
         CallbackFnVoid(true, &IVkApiCallbacks::OnRequestAppKey);
      }
      else
      {
         PrintText(TE_ERROR, "AppKey not set.");
         bHaveErrors = true;
      }
   }
   if(!CheckAppServiceKey())
   {
      if (bRunCallbacks)
      {
         CallbackFnVoid(true, &IVkApiCallbacks::OnRequestAppServiceKey);
      }
      else
      {
         PrintText(TE_ERROR, "AppServiceKey not set.");
         bHaveErrors = true;
      }
   }
  
   if (!CheckUserToken())
   {

   }
   if (bRunCallbacks)
   {
      WaitCallbacks();
   }
   return bHaveErrors;
}





void CVKApi::VkThreadFunc()
{

 
   if (!ThreadInMainThread())
   {
      SetThreadName(__FUNCTION__);
   }
   ParseVKCfg();
   CheckConfig(true);

   if (m_AppID == 0)
   {

      if (m_AppID == 0)
      {
         PrintText(TE_ERROR, "AppID==0.");
      }
   }


   if (m_sUserToken.empty())
   {
      do 
      {
         Curl().Reset();
         if (!ReceiveUserToken())
         {
            return;
         }

      } while (1);
      

   }
   if (!GetUserInfo())
   {
      return;
   }
 
   do 
   {
     
   }
   while (!IsThreadTerminating());

   PrintText(TE_DEBUG, "STOP_THREAD");
   return;
}


std::string CVKApi::SendQuery(const std::string &sMethod, const std::map<std::string, std::string> &QueryData)
{
   
   do {} while (std::chrono::duration_cast<std::chrono::milliseconds>(std::chrono::system_clock::now() - m_LastQuery).count() <334);
 

   Curl().AddPostFields(QueryData);
   Curl().AddPostField("access_token", m_sUserToken);
   Curl().AddPostField("v", m_sApiVersion);
   auto sQueryResponce = Curl().SendQuery("https://api.vk.com/method/%s", sMethod.c_str());

   m_LastQuery = std::chrono::system_clock::now();
  
   return sQueryResponce;
}



std::vector<CVKApi::AlbumInfo_t> CVKApi::GetGroupAlbums(int64_t nGroupID)
{
   std::map<std::string, std::string> QData;
   std::vector<AlbumInfo_t> vAlbums;
   QData["owner_id"] = std::to_string(-nGroupID);

   auto sQueryResponce = SendQuery("photos.getAlbums", QData);

   //captcha_key,captcha_sid
   if (!IsValidResponce(sQueryResponce))
   {
      return vAlbums;
   }
   rapidjson::Document d;
   d.Parse(sQueryResponce.c_str());
   if (!d.HasMember("response"))
   {
      return vAlbums;
   }
   auto &resp = d["response"];
   if (!resp.HasMember("items"))
   {
      return vAlbums;
   }
   if (!resp["items"].IsArray())
   {
      return vAlbums;
   }
 
   AlbumInfo_t aInfo;
   auto aItems = resp["items"].GetArray();
   for (int i = 0; i < aItems.Size(); i++)
   {
      if (aItems[i].IsObject())
      {
         memset(&aInfo, 0, sizeof(aInfo));
         if (aItems[i].HasMember("id"))
         {
            aInfo.id = aItems[i]["id"].GetInt64();
         }
         if (aItems[i].HasMember("thumb_id"))
         {
            aInfo.thumb_id = aItems[i]["thumb_id"].GetInt64();
         }
         if (aItems[i].HasMember("owner_id"))
         {
            aInfo.owner_id = aItems[i]["owner_id"].GetInt64();
         }
         if (aItems[i].HasMember("title"))
         {
            aInfo.title = aItems[i]["title"].GetString();
         }
         if (aItems[i].HasMember("description"))
         {
            aInfo.description = aItems[i]["description"].GetString();
         }


         if (aItems[i].HasMember("created"))
         {
            aInfo.created = aItems[i]["created"].GetInt();
         }
         if (aItems[i].HasMember("updated"))
         {
            aInfo.updated = aItems[i]["updated"].GetInt();
         }
         if (aItems[i].HasMember("size"))
         {
            aInfo.size = aItems[i]["size"].GetInt();
         }
         if (aItems[i].HasMember("description"))
         {
            aInfo.can_upload = aItems[i]["can_upload"].GetInt();
         }
         if (aInfo.size > 0)
         {
            vAlbums.push_back(aInfo);
         }
      }
   }
   return vAlbums;

}

CVKApi::PhotoInfo_t CVKApi::GetAlbumPhoto(const AlbumInfo_t &pAlbum, uint32_t nPhoto)
{
   PhotoInfo_t pInfo;
   memset(&pInfo, 0, sizeof(pInfo));
   if (!pAlbum.size)
   {
      return pInfo;
   }
   if (pAlbum.size < nPhoto)
   {
      nPhoto = Rand(0, pAlbum.size - 1);
   }
   std::map<std::string, std::string> QData;
   std::vector<AlbumInfo_t> vAlbums;
   QData["owner_id"] = std::to_string(pAlbum.owner_id);
   QData["album_id"] = std::to_string(pAlbum.id);
   QData["offset"] = std::to_string(nPhoto);
   QData["count"] = std::to_string(1);

   auto sQueryResponce = SendQuery("photos.get", QData);
   rapidjson::Document d;
   d.Parse(sQueryResponce.c_str());
   if (!d.HasMember("response"))
   {
      return pInfo;
   }
   auto &resp = d["response"];
   if (!resp.HasMember("items"))
   {
      return pInfo;
   }
   if (!resp["items"].IsArray())
   {
      return pInfo;
   }

 
   auto aItems = resp["items"].GetArray();
   if (aItems.Size() > 1)
   {
      return pInfo;
   }
   for (int i = 0; i < aItems.Size(); i++)
   {
      if (aItems[i].IsObject())
      {
        
         if (aItems[i].HasMember("id"))
         {
            pInfo.id = aItems[i]["id"].GetInt64();
         }
         if (aItems[i].HasMember("album_id"))
         {
            pInfo.album_id = aItems[i]["album_id"].GetInt64();
         }
         if (aItems[i].HasMember("owner_id"))
         {
            pInfo.owner_id = aItems[i]["owner_id"].GetInt64();
         }
         if (aItems[i].HasMember("user_id"))
         {
            pInfo.user_id = aItems[i]["user_id"].GetInt64();
         }
         if (aItems[i].HasMember("text"))
         {
            pInfo.text = aItems[i]["text"].GetString();
         }
        
         if (aItems[i].HasMember("date"))
         {
            pInfo.date = aItems[i]["date"].GetInt();
         }
      }
   }
   return pInfo;
}

std::map<uint64_t, CVKApi::UserInfo_t> CVKApi::GetGroupMembers(const std::string &sGroupID)
{
   std::map<uint64_t, CVKApi::UserInfo_t> vUsersID,mParseData;
   std::string sQueryResponce;
   if (m_sUserToken.empty())
   {
      return vUsersID;
   }
   int nCount = 0;
   int nCurID = 0;
   std::map<std::string, std::string> QData;

   std::string Code;
  
   int nOldCount = 0;
  
   do 
   {
      Code = "var Offset=" + std::to_string(vUsersID.size()) + "; \n"\
         "var nNumUsers=1;\n"\
         "var NumApiCalls=0;\n"\
         "var CurUsers;\n"\
         "var Users=[];\n"\
         "while(NumApiCalls<25&&Users.length<nNumUsers)\n"\
         "{\n"\
         "CurUsers=API.groups.getMembers({\"v\": \"" + m_sApiVersion + "\", \"group_id\":\"" + sGroupID + "\", \"offset\":Offset, \"count\": 1000,\"fields\":\"online, last_seen\"});\n"\
         "nNumUsers=CurUsers.count;\n"\
         "Users=Users+CurUsers.items;\n"\
         "Offset=Offset+1000;\n"\
         "NumApiCalls=NumApiCalls+1;\n"\
         "}\n"\
         "return {\"total\":nNumUsers,\"users\":Users};";
     
      QData["code"] = Code;
     
      sQueryResponce = SendQuery("execute", QData);
      if (!IsValidResponce(sQueryResponce))
      {
         printf("Invalid respounse: group \"%s\"\n", sGroupID.c_str());
         vUsersID.clear();
         return vUsersID;
      }

      rapidjson::Document d;
      d.Parse(sQueryResponce.c_str());

      if (!d.HasMember("response"))
      {
         vUsersID.clear();
         return vUsersID;
      }
      auto &resp = d["response"];
      if (!resp.HasMember("total")||!resp.HasMember("users"))
      {
         vUsersID.clear();
         return vUsersID;
      }
      nCount = resp["total"].GetInt();
      if (!resp["users"].IsArray())
      {
         vUsersID.clear();
         return vUsersID;
      }
      mParseData= ParseUsersArray(resp["users"]);
      vUsersID.insert(mParseData.begin(),mParseData.end());
      if (nCount)
      {
         printf("Received %i-%i users from %i (group %s)\n", nOldCount,vUsersID.size(), nCount, sGroupID.c_str());
         nOldCount = vUsersID.size();
      }

   } while (nCount!=vUsersID.size());
  
   return vUsersID;
   
}

bool CVKApi::GetUserInfo()
{
   if (m_sUserToken.empty())
   {
      return false;
   }
   std::map<std::string, std::string> QData;

   auto sQueryResponce = SendQuery("users.get", QData);
   //captcha_key,captcha_sid
   if (!IsValidResponce(sQueryResponce))
   {
      return false;
   }

   rapidjson::Document d;
   d.Parse(sQueryResponce.c_str());
   if (!d.HasMember("response")||!d["response"].IsArray()||d["response"].GetArray().Size()!=1|| !d["response"][0].HasMember("id"))
   {
      return false;
   }
   SetUserID(d["response"][0]["id"].GetUint());
   return true;

}

bool CVKApi::JoinChat(uint32_t nChatID)
{
   if (m_sUserToken.empty())
   {
      return false;
   }
   std::map<std::string, std::string> QData;

   QData["chat_id"] = std::to_string(nChatID);
   QData["user_id"] = std::to_string(GetUserID());

   auto sQueryResponce = SendQuery("messages.addChatUser", QData);

   //captcha_key,captcha_sid
   if (!IsValidResponce(sQueryResponce))
   {
      return false;
   }
   return true;
}

bool CVKApi::SetChatTitle(uint32_t nChatID, const std::string &sTitle)
{
   if (m_sUserToken.empty())
   {
      return false;
   }
   std::map<std::string, std::string> QData;

   QData["chat_id"] = std::to_string(nChatID);
   QData["title"] = sTitle;

   auto sQueryResponce = SendQuery("messages.editChat", QData);
  
   if (!IsValidResponce(sQueryResponce))
   {
      return false;
   }
   return true;
}

bool CVKApi::LeaveChat(uint32_t nChatID)
{
   if (m_sUserToken.empty())
   {
      return false;
   }
   std::map<std::string, std::string> QData;
   
   QData["chat_id"] = std::to_string(nChatID);
   QData["user_id"] = std::to_string(GetUserID());
   
   auto sQueryResponce = SendQuery("messages.removeChatUser", QData);
  
   //captcha_key,captcha_sid
   if (!IsValidResponce(sQueryResponce))
   {
      return false;
   }
   return true;
}



bool CVKApi::SendChatMessage(bool bChat, uint32_t nChatID, const std::string &sText)
{
   if (m_sUserToken.empty())
   {
      return false;
   }
   uint32_t rand = Rand(0,0xFFFFFFF);
   PrintText(TE_DEBUG, "Rand=%u\n", rand);

   std::map<std::string, std::string> QData;

   QData["peer_id"] = std::to_string(nChatID + (bChat ? 2000000000:0));
   QData["random_id"] = std::to_string(rand);
  
   
   QData["message"] = sText;
   
   auto sQueryResponce =SendQuery("messages.send",QData);

   //captcha_key,captcha_sid
   if (!IsValidResponce(sQueryResponce))
   {
      return false;
   }
   return true;
}


CVKLongPoll::LongPollParams_t CVKApi::GetLongPollServer()
{
   std::map<std::string, std::string> QData;
   CVKLongPoll::LongPollParams_t LP;
   LP.ts = 0;
   LP.pts = 0;
   QData["lp_version"] = std::to_string(2);
   QData["need_pts"] = std::to_string(1);
   auto sQueryResponce = SendQuery("messages.getLongPollServer", QData);

   //captcha_key,captcha_sid
   if (!IsValidResponce(sQueryResponce))
   {
      return LP;
   }
   rapidjson::Document d;
   d.Parse(sQueryResponce.c_str());
   if (!d.HasMember("response") || !d["response"].IsObject() || !d["response"].HasMember("key") || !d["response"].HasMember("server") || !d["response"].HasMember("ts"))
   {
      return LP;
   }
   if (d["response"].HasMember("pts"))
   {
      LP.pts = d["response"]["pts"].GetUint();
   }
   LP.ts = d["response"]["ts"].GetUint();
   LP.Server = d["response"]["server"].GetString();
   LP.Key = d["response"]["key"].GetString();
   return LP;
}

CVKApi::ChatInfo_t CVKApi::GetChatInfo(uint32_t nChatID)
{
   ChatInfo_t ChatInfo;
   ChatInfo.admin_id = 0;
   ChatInfo.vUsers.clear();
   std::map<std::string, std::string> QData;
   QData["chat_id"] = std::to_string(nChatID);
   QData["fields"] = "online,last_seen,deactivated";
   auto sQueryResponce = SendQuery("messages.getChat", QData);

   //captcha_key,captcha_sid
   if (!IsValidResponce(sQueryResponce))
   {
      return ChatInfo;
   }
   rapidjson::Document d;
   d.Parse(sQueryResponce.c_str());
   if (!d.HasMember("response"))
   {
      return ChatInfo;
   }
   auto &resp = d["response"];


   if (!resp.HasMember("users") || !resp.HasMember("admin_id"))
   {
      return ChatInfo;
   }
   

   if (!resp["users"].IsArray())
   {
      return ChatInfo;
   }
   ChatInfo.admin_id = resp["admin_id"].GetInt64();

   ChatInfo.vUsers= ParseUsersArray(resp["users"]);
  
   return ChatInfo;
}


std::map<uint64_t, CVKApi::UserInfo_t> CVKApi::ParseUsersArray(const rapidjson::Value &Users)
{
   std::map<uint64_t, CVKApi::UserInfo_t> mUsers;
  
   UserInfo_s uInfo;
   if (!Users.IsArray())
   {
      return mUsers;
   }
   auto aUsers = Users.GetArray();

   for (int i = 0; i < aUsers.Size(); i++)
   {
      uint64_t nID = 0;
      if (aUsers[i].IsObject())
      {
         memset(&uInfo, 0, sizeof(uInfo));
         if (aUsers[i].HasMember("id"))
         {
            nID = aUsers[i]["id"].GetInt64();
         }
         if (aUsers[i].HasMember("first_name"))
         {
            uInfo.first_name = aUsers[i]["first_name"].GetString();
         }
         if (aUsers[i].HasMember("last_name"))
         {
            uInfo.last_name = aUsers[i]["last_name"].GetString();
         }
         if (aUsers[i].HasMember("online"))
         {
            uInfo.online = aUsers[i]["online"].GetInt();
         }
         if (aUsers[i].HasMember("last_seen") && aUsers[i]["last_seen"].IsObject())
         {
            if (aUsers[i]["last_seen"].HasMember("time"))
            {
               uInfo.Last_time = aUsers[i]["last_seen"]["time"].GetUint();
            }
            if (aUsers[i]["last_seen"].HasMember("platform"))
            {
               uInfo.last_patform = aUsers[i]["last_seen"]["platform"].GetUint();
            }
         }
         if (aUsers[i].HasMember("deactivated"))
         {
            uInfo.deactivated = UserInfo_s::DA_DELETED;
            if (aUsers[i]["deactivated"].IsString())
            {
               std::string Da_Type = aUsers[i]["deactivated"].GetString();
               if (Da_Type == "deleted")
               {
                  uInfo.deactivated = UserInfo_s::DA_DELETED;
               }
               if (Da_Type == "banned")
               {
                  uInfo.deactivated = UserInfo_s::DA_BANNED;
               }
            }
           
            uInfo.last_name = aUsers[i]["last_name"].GetString();
         }
         if (nID)
         {
            mUsers[nID] = uInfo;
         }
         
      }
   }
   return mUsers;
}



void CVKApi::Free()
{
   StopVKThread();
   PrintText(TE_DEBUG, "EXIT");
   delete this;
}

int CVKApi::Rand(int nMin, int nMax)
{
   std::random_device rd;
   if (nMax < nMin)
   {
      std::swap(nMin, nMax);
   }

   static std::mt19937 gen(time(0));
   std::uniform_int_distribution<int> uid1(nMin, nMax);
   return uid1(gen);
}




