/***************************************************************************
 * 
 * $Id$
 * 
 **************************************************************************/

/**
 * @file $HeadURL$
 * @author $Author$(hoping@baimashi.com)
 * @date $Date$
 * @version $Revision$
 * @brief 
 *  
 **/

#include "stdafx.h"
#include <boost/locale.hpp>


CNode::CNode(GumboNode* apNode):mpNode(apNode)
{
   
   try
   {
      mTag = tag();
      mText = text();
      mOwnText = ownText();
      
   }
   catch (const std::exception& e)
   {
      std::cout << "Error:" << e.what() << std::endl;
   }
   mNumChilds = childNum();
   mAttributes = GetAttributes();
   
 
}

CNode::~CNode()
{
}

CNode CNode::parent()
{
	return CNode(mpNode->parent);
}

CNode CNode::nextSibling()
{
	return parent().childAt(mpNode->index_within_parent + 1);
}

CNode CNode::prevSibling()
{
	return parent().childAt(mpNode->index_within_parent - 1);
}

unsigned int CNode::childNum() const
{
	if (mpNode->type != GUMBO_NODE_ELEMENT)
	{
		return 0;
	}

	return mpNode->v.element.children.length;

}

bool CNode::valid()
{
	return mpNode != NULL;
}

CNode CNode::childAt(size_t i) const
{
	if (mpNode->type != GUMBO_NODE_ELEMENT || i >= mpNode->v.element.children.length)
	{
		return CNode();
	}

	return CNode((GumboNode*) mpNode->v.element.children.data[i]);
}

std::string CNode::attribute(std::string key)
{
	if (mpNode->type != GUMBO_NODE_ELEMENT)
	{
		return "";
	}

	GumboVector attributes = mpNode->v.element.attributes;
	for (unsigned int i = 0; i < attributes.length; i++)
	{
		GumboAttribute* attr = (GumboAttribute*) attributes.data[i];
		if (key == attr->name)
		{
         return FromUTF(attr->value);
		}
	}

	return "";
}

std::string CNode::text()
{
   return FromUTF(CQueryUtil::nodeText(mpNode));
}

std::string CNode::ownText()
{
   return FromUTF(CQueryUtil::nodeOwnText(mpNode));
}

size_t CNode::startPos()
{
	switch(mpNode->type)
	{
	  case GUMBO_NODE_ELEMENT:
		  return mpNode->v.element.start_pos.offset + mpNode->v.element.original_tag.length;
	  case GUMBO_NODE_TEXT:
		  return mpNode->v.text.start_pos.offset;
	  default:
		  return 0;
  }
}

size_t CNode::endPos()
{
	switch(mpNode->type)
	{
	  case GUMBO_NODE_ELEMENT:
		  return mpNode->v.element.end_pos.offset;
	  case GUMBO_NODE_TEXT:
		  return mpNode->v.text.original_text.length + startPos();
	  default:
		  return 0;
	}
}

size_t CNode::startPosOuter()
{
	switch(mpNode->type)
	{
	case GUMBO_NODE_ELEMENT:
		return mpNode->v.element.start_pos.offset;
	case GUMBO_NODE_TEXT:
		return mpNode->v.text.start_pos.offset;
	default:
		return 0;
	}
}

size_t CNode::endPosOuter()
{
	switch(mpNode->type)
	{
	case GUMBO_NODE_ELEMENT:
		return mpNode->v.element.end_pos.offset + mpNode->v.element.original_end_tag.length;
	case GUMBO_NODE_TEXT:
		return mpNode->v.text.original_text.length + startPos();
	default:
		return 0;
	}
}

std::string CNode::tag()
{
	if (!mpNode||mpNode->type != GUMBO_NODE_ELEMENT)
	{
		return "";
	}

	return gumbo_normalized_tagname(mpNode->v.element.tag);
}

CSelection CNode::find(std::string aSelector)
{
	CSelection c(mpNode);
	return c.find(aSelector);
}

std::vector<std::pair<std::string, std::string>> CNode::GetAttributes() const
{
   std::vector<std::pair<std::string, std::string>> vAttrib;
   if (mpNode->type == GUMBO_NODE_ELEMENT)
   {

      GumboVector attributes = mpNode->v.element.attributes;
      for (unsigned int i = 0; i < attributes.length; i++)
      {
         GumboAttribute* attr = (GumboAttribute*)attributes.data[i];
         ;
         vAttrib.push_back(std::pair<std::string, std::string>(FromUTF(attr->name), FromUTF(attr->value)));
      }
   }
   return vAttrib;
}

std::string CNode::FromUTF(const std::string &str) const
{
   return boost::locale::conv::from_utf(str, "cp1251");
}

/* vim: set ts=4 sw=4 sts=4 tw=100 noet: */
