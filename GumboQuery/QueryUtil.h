/***************************************************************************
 * 
 * $Id$
 * 
 **************************************************************************/

/**
 * @file $HeadURL$
 * @author $Author$(hoping@baimashi.com)
 * @date $Date$
 * @version $Revision$
 * @brief 
 *  
 **/

#ifndef QUERYUTIL_H_
#define QUERYUTIL_H_


class CQueryUtil
{
	public:

      static std::string tolower(const std::string &);
      static std::vector<CNode> unionNodes(const std::vector<CNode> &aNodes1, const std::vector<CNode> &aNodes2);
      static bool nodeExists(std::vector<CNode> aNodes, const CNode &apNode);
		static std::string nodeText(GumboNode* apNode);
    
		static std::string nodeOwnText(GumboNode* apNode);
      
	private:

		static void writeNodeText(GumboNode* apNode, std::string& aText);
    

};

#endif /* QUERYUTIL_H_ */

/* vim: set ts=4 sw=4 sts=4 tw=100 noet: */
