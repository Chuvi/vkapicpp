/***************************************************************************
 * 
 * $Id$
 * 
 **************************************************************************/

/**
 * @file $HeadURL$
 * @author $Author$(hoping@baimashi.com)
 * @date $Date$
 * @version $Revision$
 * @brief 
 *  
 **/

#ifndef SELECTION_H_
#define SELECTION_H_


class CNode;

class CSelection: public CObject
{
private:
   std::vector<CNode> mNodes;
	public:
      CSelection(const CSelection& pOther):mNodes(pOther.mNodes)
      {

      }
		CSelection(const CNode &apNode);

      CSelection(const std::vector<CNode> &aNodes);

		virtual ~CSelection();

	public:

		CSelection find(std::string aSelector);

		CNode nodeAt(size_t i);

		size_t nodeNum();
      auto begin()->decltype(mNodes.begin())
      {
        return mNodes.begin();
      }

      auto end()->decltype(mNodes.end())
      {
         return mNodes.end();
      }

      auto cbegin()->decltype(mNodes.cbegin()) const
      {
         return mNodes.cbegin();
      }

      auto cend()->decltype(mNodes.cend()) const
      {
         return mNodes.cend();
      }
	private:

		
};

#endif /* SELECTION_H_ */

/* vim: set ts=4 sw=4 sts=4 tw=100 noet: */
