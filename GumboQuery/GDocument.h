/***************************************************************************
 * 
 * $Id$
 * 
 **************************************************************************/

/**
 * @file $HeadURL$
 * @author $Author$(hoping@baimashi.com)
 * @date $Date$
 * @version $Revision$
 * @brief 
 *  
 **/

#ifndef DOCUMENT_H_
#define DOCUMENT_H_



class CDocument: public CObject
{
	public:

		CDocument();

		void parse(const std::string& aInput);

		virtual ~CDocument();

		CSelection find(std::string aSelector);

	private:

		void reset();

	private:

		GumboOutput* mpOutput;
};

#else
#error DEFINED
#endif /* DOCUMENT_H_ */

/* vim: set ts=4 sw=4 sts=4 tw=100 noet: */
