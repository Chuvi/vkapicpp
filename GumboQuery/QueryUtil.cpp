/***************************************************************************
 * 
 * $Id$
 * 
 **************************************************************************/

/**
 * @file $HeadURL$
 * @author $Author$(hoping@baimashi.com)
 * @date $Date$
 * @version $Revision$
 * @brief 
 *  
 **/

#include "stdafx.h"
#include <algorithm>
#include <locale>
std::string CQueryUtil::tolower(const std::string &s)
{
	/*for (unsigned int i = 0; i < s.size(); i++)
	{
		char c = s[i];
		if (c >= 'A' && c <= 'Z')
		{
			c = 'a' + c - 'A';
			s[i] = c;
		}
	}
   return s;
   */
   std::string Str = s;
   std::transform(Str.begin(), Str.end(), Str.begin(), ::tolower);
   return Str;
}


#if 0
bool CQueryUtil::nodeExists(std::vector<GumboNode*> aNodes, GumboNode* apNode)
{
	for (std::vector<GumboNode*>::iterator it = aNodes.begin(); it != aNodes.end(); it++)
	{
		GumboNode* pNode = *it;
		if (pNode == apNode)
		{
			return true;
		}
	}
	return false;
}

#endif
std::vector<CNode> CQueryUtil::unionNodes(const std::vector<CNode> &aNodes1, const std::vector<CNode> &aNodes2)
{
   std::vector<CNode> resNodes(aNodes1);
   for (auto Node : aNodes2)
   {
      if (nodeExists(resNodes, Node))
      {
         continue;
      }

      resNodes.push_back(Node);
   }

   return resNodes;
}


bool CQueryUtil::nodeExists(std::vector<CNode> aNodes, const CNode &apNode)
{
   for (auto Node:aNodes)
   {
     
      if (Node == apNode)
      {
         return true;
      }
   }
   return false;
}

std::string CQueryUtil::nodeText(GumboNode* apNode)
{
	std::string text;
	writeNodeText(apNode, text);
	return text;
}

std::string CQueryUtil::nodeOwnText(GumboNode* apNode)
{
	std::string text;
	if (apNode->type != GUMBO_NODE_ELEMENT)
	{
		return text;
	}

	GumboVector children = apNode->v.element.children;
	for (unsigned int i = 0; i < children.length; i++)
	{
		GumboNode* child = (GumboNode*) children.data[i];
		if (child->type == GUMBO_NODE_TEXT)
		{
			text.append(child->v.text.text);
		}
	}

	return text;
}

void CQueryUtil::writeNodeText(GumboNode* apNode, std::string& aText)
{
	switch (apNode->type)
	{
		case GUMBO_NODE_TEXT:
			aText.append(apNode->v.text.text);
			break;
		case GUMBO_NODE_ELEMENT:
		{
			GumboVector children = apNode->v.element.children;
			for (unsigned int i = 0; i < children.length; i++)
			{
				GumboNode* child = (GumboNode*) children.data[i];
				writeNodeText(child, aText);
			}
			break;
		}
		default:
			break;
	}
}

/* vim: set ts=4 sw=4 sts=4 tw=100 noet: */
