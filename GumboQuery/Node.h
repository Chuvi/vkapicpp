/***************************************************************************
 * 
 * $Id$
 * 
 **************************************************************************/

/**
 * @file $HeadURL$
 * @author $Author$(hoping@baimashi.com)
 * @date $Date$
 * @version $Revision$
 * @brief 
 *  
 **/

#ifndef NODE_H_
#define NODE_H_

class CSelection;

class CNode
{
	public:

		CNode(GumboNode* apNode = NULL);

		virtual ~CNode();

	public:

		bool valid();

		CNode parent();

		CNode nextSibling();

		CNode prevSibling();

		unsigned int childNum() const;

		CNode childAt(size_t i) const;

		std::string attribute(std::string key);

		std::string text();

		std::string ownText();

		size_t startPos();

		size_t endPos();

		size_t startPosOuter();

		size_t endPosOuter();

		std::string tag();

		CSelection find(std::string aSelector);

      bool operator ==(const CNode &pOther) const
      {
         return mpNode == pOther.mpNode;
      }
      bool operator !=(const CNode &pOther) const
      {
         return mpNode != pOther.mpNode;
      }
      GumboNodeType getType() const
      {
         return mpNode->type;
      }
      const GumboNode* GetNode() const
      {
         return mpNode;
      }

      std::vector<std::pair<std::string, std::string>> GetAttributes() const;
	private:
      std::string FromUTF(const std::string &str) const;
      std::string mTag;
      std::string mText;
      std::string mOwnText;
      std::vector<std::pair<std::string, std::string>> mAttributes;
      int mNumChilds;
      GumboNode* mpNode;
      
};

#endif /* NODE_H_ */

/* vim: set ts=4 sw=4 sts=4 tw=100 noet: */
