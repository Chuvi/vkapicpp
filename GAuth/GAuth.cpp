#include "GAuth.h"
#include <string.h>
#include "base32.h"
#include "HMAC_SHA1.h"
#include <time.h>


// #define SECRET_BITS               80          // Must be divisible by eight
 #define VERIFICATION_CODE_MODULUS (1000*1000) // Six digits
// #define SCRATCHCODES              5           // Number of initial scratchcodes
// #define SCRATCHCODE_LENGTH        8           // Eight digits per scratchcode
// #define BYTES_PER_SCRATCHCODE     4           // 32bit of randomness is enough
 #define BITS_PER_BASE32_CHAR      5           // Base32 expands space by 8/5

uint32_t GenerateAuthCode(const char *key, uint32_t tm)
{
  
   uint8_t challenge[8];
   uint8_t secret[100];
   uint8_t hash[CHMAC_SHA1::SHA1_DIGEST_LENGTH];
   uint32_t truncatedHash = 0;
   int offset = 0;
   if (tm == 0)
   {
      tm = static_cast<uint32_t>(time(0) / 30);
   }
   for (int i = 8; i--; tm >>= 8)
   {
      challenge[i] = tm;
   }
   int secretLen = (strlen(key) + 7) / 8 * BITS_PER_BASE32_CHAR;
   if (secretLen <= 0 || secretLen > 100)
   {
      return 0xFFFFFFFF;
   }
  
   if ((secretLen = base32_decode((const uint8_t *)key, secret, secretLen)) < 1)
   {
      return 0xFFFFFFFF;
   }
   CHMAC_SHA1().HMAC_SHA1(challenge, 8, secret, secretLen, hash);
   offset = hash[CHMAC_SHA1::SHA1_DIGEST_LENGTH - 1] & 0xF;
   for (int i = 0; i < 4; ++i)
   {
      truncatedHash <<= 8;
      truncatedHash |= hash[offset + i];
   }
   truncatedHash &= 0x7FFFFFFF;
   truncatedHash %= VERIFICATION_CODE_MODULUS;

   return truncatedHash;
}