#ifndef GAuth_h__
#define GAuth_h__
#include <stdint.h>

uint32_t GenerateAuthCode(const char *key, uint32_t tm = 0);
#endif // GAuth_h__